"use strict";
    
    var timeout = {
        settings: {time: 100000},
        session: false,
        init: function(){
            var self = this;
            
            document.onmousemove = function(){
                clearTimeout(self.session);
                self.session = setTimeout(function(){
                    document.location.href = "lockscreen";
                }, self.settings.time);
            }
        }
    };
    
$(function(){
    timeout.init();
});      