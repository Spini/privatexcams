@extends('layouts.default')
@section('content')
    <div class="wrapper">
        <div class="row">
            <div class="col-md-6">
                                    <!-- google map with marker -->
                                    <div class="page-subtitle page-subtitle-centralized">
                                        <h3>Google Map With Marker</h3>
                                        <p>Place your markers wherever you want</p>
                                    </div>
                                    <div class="map-wrapper">
                                        <div id="google_ptm_map" style="width: 100%; height: 300px;"></div>
                                    </div>
                                    <!-- ./google map with marker -->
                                </div>
        </div>
    </div>
@endsection