@extends('layouts.default')
@section('content')
    <div class="wrapper wrapper-white">
        <div class="row">
            <div class="col-md-6">
                <!-- google europe map -->
                <div class="page-subtitle page-subtitle-centralized">
                    <h3>Google Europe Map</h3>
                    <p>Europe map sample, really helpful feature</p>
                </div>
                <div class="map-wrapper">
                    <div id="google_eu_map" style="width: 100%; height: 300px;"></div>
                </div>
                <!-- ./google europe map -->
            </div>
        </div>
    </div>
@endsection