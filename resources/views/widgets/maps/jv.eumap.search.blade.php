@extends('layouts.default')
@section('content')
    <div class="wrapper">
        <div class="row">
            <div class="col-md-6">
                <!-- jvectormap usa -->
                <div class="page-subtitle page-subtitle-centralized">
                    <h3>jVectorMap Europe</h3>
                    <p>Vector Europe map sample</p>
                </div>
                <div class="map-wrapper">
                    <div class="form-group map-wrapper-field">
                        <div class="col-md-3 col-md-offset-9">
                            <input type="text" class="form-control" placeholder="Select state"
                                   id="vector_usa_map_value"/>
                        </div>
                    </div>
                    <div id="vector_usa_map" style="width: 100%; height: 300px;"></div>
                </div>
                <!-- ./jvectormap usa -->
            </div>
        </div>
    </div>
@endsection