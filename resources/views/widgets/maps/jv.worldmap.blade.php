@extends('layouts.default')
@section('content')
    <div class="wrapper wrapper-white">
        <div class="row">
            <div class="col-md-12">
                <!-- jvectormap world -->
                <div class="page-subtitle page-subtitle-centralized">
                    <h3>jVectorMap World</h3>
                    <p>Sample of world vector map</p>
                </div>
                <div class="map-wrapper">
                    <div class="form-group map-wrapper-field">
                        <div class="col-md-3 col-md-offset-9">
                            <input type="text" class="form-control" placeholder="Select country"
                                   id="vector_world_map_value"/>
                        </div>
                    </div>
                    <div id="vector_world_map" style="width: 100%; height: 300px"></div>
                </div>
                <!-- ./jvectormap world -->
            </div>
        </div>
    </div>
@endsection