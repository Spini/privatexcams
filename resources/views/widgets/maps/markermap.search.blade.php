@extends('layouts.default')
@section('content')
    <div class="wrapper">
        <div class="row">
            <div class="col-md-6">
                <!-- google map with search -->
                <div class="page-subtitle page-subtitle-centralized">
                    <h3>Google Map With Marker</h3>
                    <p>Place your markers wherever you want</p>
                </div>
                <div class="map-wrapper">
                    <div class="form-group map-wrapper-field">
                        <div class="col-md-3 col-md-offset-9">
                            <input type="text" id="target" class="form-control"/>
                        </div>
                    </div>
                    <div id="google_search_map" style="width: 100%; height: 300px;"></div>
                </div>
                <!-- ./google map with search -->
            </div>
        </div>
    </div>
@endsection