@extends('layouts.default')
@section('content')
    <div class="wrapper">
        <div class="row">
            <div class="col-md-6">
                <!-- jvectormap europe -->
                <div class="page-subtitle page-subtitle-centralized">
                    <h3>jVectorMap Europe</h3>
                    <p>Vector Europe map sample</p>
                </div>
                <div class="map-wrapper">
                    <div id="vector_europe_map" style="width: 100%; height: 300px;"></div>
                </div>
                <!-- ./jvectormap europe -->
            </div>
        </div>
    </div>
@endsection