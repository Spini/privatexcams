@extends('layouts.default')
@section('content')
    <div class="wrapper wrapper-white">
        <div class="row">
            <div class="col-md-6">
                <!-- google world map -->
                <div class="page-subtitle page-subtitle-centralized">
                    <h3>Google World Map</h3>
                    <p>Default sample of google world map</p>
                </div>
                <div class="map-wrapper">
                    <div id="google_world_map" style="width: 100%; height: 300px;"></div>
                </div>
                <!-- ./google world map -->
            </div>
        </div>
    </div>
@endsection