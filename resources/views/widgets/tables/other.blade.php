@extends('layouts.default')
@section('content')
    <div class="dev-page-content">
        <!-- page content container -->
        <div class="container">

            <!-- page title -->
            <div class="page-title">
                <h1>Tables Default</h1>
                <p>One of the greatest editors that has ever made</p>

                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Tables</a></li>
                    <li>Tables Other</li>
                </ul>
            </div>
            <!-- ./page title -->

            <!-- column control -->
            <div class="wrapper wrapper-white">
                <div class="page-subtitle">
                    <h3>Column Control</h3>
                    <p>Using custom markup you can add table controls, such as column control.</p>
                </div>

                <div class="table-responsive">
                    <div class="table-controls">
                        <div class="table-controls-block"></div>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>first</th>
                                <th>second</th>
                                <th>third</th>
                                <th>fourth</th>
                                <th>fifth</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>0</td>
                                <td>Lorem</td>
                                <td>Ipsum</td>
                                <td>Dolor</td>
                                <td>Sit</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Lorem</td>
                                <td>Ipsum</td>
                                <td>Dolor</td>
                                <td>Sit</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Lorem</td>
                                <td>Ipsum</td>
                                <td>Dolor</td>
                                <td>Sit</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Lorem</td>
                                <td>Ipsum</td>
                                <td>Dolor</td>
                                <td>Sit</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Lorem</td>
                                <td>Ipsum</td>
                                <td>Dolor</td>
                                <td>Sit</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- ./column control -->

            <!-- Copyright -->
            <div class="copyright">
                <div class="pull-left">
                    &copy; 2015 <strong>Aqvatarius</strong>. All rights reserved.
                </div>
                <div class="pull-right">
                    <a href="#">Terms of use</a> / <a href="#">Privacy Policy</a>
                </div>
            </div>
            <!-- ./Copyright -->

        </div>
        <!-- ./page content container -->
    </div>
@endsection