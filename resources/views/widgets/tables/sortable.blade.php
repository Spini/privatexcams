@extends('layouts.default')
@section('content')
    <div class="dev-page-content">
        <!-- page content container -->
        <div class="container">

            <!-- page title -->
            <div class="page-title">
                <h1>Sortable Tables</h1>
                <p>Sortable features for default tables</p>

                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Tables</a></li>
                    <li>Sortable tables</li>
                </ul>
            </div>
            <!-- ./page title -->

            <!-- datatables plugin -->
            <div class="wrapper wrapper-white">
                <div class="page-subtitle">
                    <h3>DataTables</h3>
                    <p>DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, based
                        upon the foundations of progressive enhancement, and will add advanced interaction controls to
                        any HTML table.</p>
                    <p>Add class <code>.table-sortable</code> to table to get zero configuration.</p>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-sortable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Age</th>
                            <th>Start date</th>
                            <th>Salary</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Tiger Nixon</td>
                            <td>System Architect</td>
                            <td>Edinburgh</td>
                            <td>61</td>
                            <td>2011/04/25</td>
                            <td>$320,800</td>
                        </tr>
                        <tr>
                            <td>Garrett Winters</td>
                            <td>Accountant</td>
                            <td>Tokyo</td>
                            <td>63</td>
                            <td>2011/07/25</td>
                            <td>$170,750</td>
                        </tr>
                        <tr>
                            <td>Ashton Cox</td>
                            <td>Junior Technical Author</td>
                            <td>San Francisco</td>
                            <td>66</td>
                            <td>2009/01/12</td>
                            <td>$86,000</td>
                        </tr>
                        <tr>
                            <td>Cedric Kelly</td>
                            <td>Senior Javascript Developer</td>
                            <td>Edinburgh</td>
                            <td>22</td>
                            <td>2012/03/29</td>
                            <td>$433,060</td>
                        </tr>
                        <tr>
                            <td>Airi Satou</td>
                            <td>Accountant</td>
                            <td>Tokyo</td>
                            <td>33</td>
                            <td>2008/11/28</td>
                            <td>$162,700</td>
                        </tr>
                        <tr>
                            <td>Brielle Williamson</td>
                            <td>Integration Specialist</td>
                            <td>New York</td>
                            <td>61</td>
                            <td>2012/12/02</td>
                            <td>$372,000</td>
                        </tr>
                        <tr>
                            <td>Herrod Chandler</td>
                            <td>Sales Assistant</td>
                            <td>San Francisco</td>
                            <td>59</td>
                            <td>2012/08/06</td>
                            <td>$137,500</td>
                        </tr>
                        <tr>
                            <td>Rhona Davidson</td>
                            <td>Integration Specialist</td>
                            <td>Tokyo</td>
                            <td>55</td>
                            <td>2010/10/14</td>
                            <td>$327,900</td>
                        </tr>
                        <tr>
                            <td>Colleen Hurst</td>
                            <td>Javascript Developer</td>
                            <td>San Francisco</td>
                            <td>39</td>
                            <td>2009/09/15</td>
                            <td>$205,500</td>
                        </tr>
                        <tr>
                            <td>Sonya Frost</td>
                            <td>Software Engineer</td>
                            <td>Edinburgh</td>
                            <td>23</td>
                            <td>2008/12/13</td>
                            <td>$103,600</td>
                        </tr>
                        <tr>
                            <td>Jena Gaines</td>
                            <td>Office Manager</td>
                            <td>London</td>
                            <td>30</td>
                            <td>2008/12/19</td>
                            <td>$90,560</td>
                        </tr>
                        <tr>
                            <td>Quinn Flynn</td>
                            <td>Support Lead</td>
                            <td>Edinburgh</td>
                            <td>22</td>
                            <td>2013/03/03</td>
                            <td>$342,000</td>
                        </tr>
                        <tr>
                            <td>Charde Marshall</td>
                            <td>Regional Director</td>
                            <td>San Francisco</td>
                            <td>36</td>
                            <td>2008/10/16</td>
                            <td>$470,600</td>
                        </tr>
                        <tr>
                            <td>Haley Kennedy</td>
                            <td>Senior Marketing Designer</td>
                            <td>London</td>
                            <td>43</td>
                            <td>2012/12/18</td>
                            <td>$313,500</td>
                        </tr>
                        <tr>
                            <td>Tatyana Fitzpatrick</td>
                            <td>Regional Director</td>
                            <td>London</td>
                            <td>19</td>
                            <td>2010/03/17</td>
                            <td>$385,750</td>
                        </tr>
                        <tr>
                            <td>Michael Silva</td>
                            <td>Marketing Designer</td>
                            <td>London</td>
                            <td>66</td>
                            <td>2012/11/27</td>
                            <td>$198,500</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- ./datatables plugin -->

            <!-- Sortable plugin -->
            <div class="wrapper">
                <div class="page-subtitle">
                    <h3>Sortable</h3>
                    <p>Add attribute <code>data-sortable</code> to table to get simple sortable table.</p>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped" data-sortable>
                        <thead>
                        <tr>
                            <th class="sort-header">#</th>
                            <th class="sort-header">Name</th>
                            <th class="sort-header">Birthday</th>
                            <th class="sort-header">Grocery item</th>
                            <th class="sort-header">Price</th>
                            <th class="sort-header">Version</th>
                            <th class="sort-header">Filesize</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Gonzo the Great</td>
                            <td>12-2-70</td>
                            <td>Radishes</td>
                            <td>$0.63</td>
                            <td>31.0.1650.57</td>
                            <td>124k</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Ernie</td>
                            <td>10/11/69</td>
                            <td>Fish</td>
                            <td>$12.09</td>
                            <td>11.0.1</td>
                            <td>134.56 GB</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Bert</td>
                            <td>10/11/1969</td>
                            <td>Broccoli</td>
                            <td>$0.79</td>
                            <td>18.0.1284.49</td>
                            <td>134 B</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Cookie Monster</td>
                            <td>1/09/1966</td>
                            <td>Oreo cookies</td>
                            <td>$2.78</td>
                            <td>3.0.0</td>
                            <td>4.13 TiB</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Kermit</td>
                            <td>12-05-1955</td>
                            <td>Granola</td>
                            <td>$4.73</td>
                            <td>3.0.9</td>
                            <td>12 YB</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Sam the Eagle</td>
                            <td>04/07/75</td>
                            <td>Corn dog</td>
                            <td>$5.50</td>
                            <td>3.1.2</td>
                            <td>19.4 K</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- summernote plugin -->

            <!-- Copyright -->
            <div class="copyright">
                <div class="pull-left">
                    &copy; 2015 <strong>Aqvatarius</strong>. All rights reserved.
                </div>
                <div class="pull-right">
                    <a href="#">Terms of use</a> / <a href="#">Privacy Policy</a>
                </div>
            </div>
            <!-- ./Copyright -->

        </div>
        <!-- ./page content container -->
    </div>
@endsection