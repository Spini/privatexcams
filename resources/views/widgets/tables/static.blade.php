@extends('layouts.default')
@section('content')
    <div class="dev-page-content">
        <!-- page content container -->
        <div class="container">

            <!-- page title -->
            <div class="page-title">
                <h1>Static Tables</h1>
                <p>Default samples of table using</p>

                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Tables</a></li>
                    <li>Static Tables</li>
                </ul>
            </div>
            <!-- ./page title -->

            <!-- basic table -->
            <div class="wrapper wrapper-white">
                <div class="page-subtitle">
                    <h3>Basic example</h3>
                    <p>For basic styling - light padding and only horizontal dividers - add the base class
                        <code>.table</code> to any <code>&lt;table></code>.</p>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- ./basic table -->

            <!-- striped rows -->
            <div class="wrapper">
                <div class="page-subtitle">
                    <h3>Striped rows</h3>
                    <p>Use <code>.table-striped</code> to add zebra-striping to any table row within the <code>&lt;tbody></code>.
                    </p>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- ./striped rows -->

            <!-- bordered table -->
            <div class="wrapper wrapper-white">
                <div class="page-subtitle">
                    <h3>Bordered table</h3>
                    <p>Add <code>.table-bordered</code> for borders on all sides of the table and cells.</p>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- ./bordered table -->

            <!-- hover rows table -->
            <div class="wrapper">
                <div class="page-subtitle">
                    <h3>Hover Table</h3>
                    <p>Add <code>.table-hover</code> to enable a hover state on table rows within a <code>
                            &lt;tbody></code>.</p>
                </div>

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- ./hover rows table -->


            <!-- All features -->
            <div class="wrapper wrapper-white">
                <div class="page-subtitle">
                    <h3>All Table Features</h3>
                    <p>Add <code>.table-bordered</code>, <code>.table-striped</code>, <code>.table-hover</code> to get
                        all features.</p>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- ./all features -->

            <!-- Copyright -->
            <div class="copyright">
                <div class="pull-left">
                    &copy; 2015 <strong>Aqvatarius</strong>. All rights reserved.
                </div>
                <div class="pull-right">
                    <a href="#">Terms of use</a> / <a href="#">Privacy Policy</a>
                </div>
            </div>
            <!-- ./Copyright -->

        </div>
        <!-- ./page content container -->
    </div>
@endsection