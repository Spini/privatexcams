@extends('layouts.app')
@section('page-content')
    <div class="dev-page dev-page-login">
        <div class="dev-page-login-block">
            <a class="dev-page-login-block__logo">Intuitive</a>
            <div class="dev-page-login-block__form">
                <div class="title"><strong>Welcome</strong>, please login</div>
                <form role="form" action="{{ url('/login') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                   placeholder="Login" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
<strong>{{ $errors->first('email') }}</strong>
</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input id="password" type="password" class="form-control" name="password"
                                   placeholder="Password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
<strong>{{ $errors->first('password') }}</strong>
</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group no-border margin-top-20">
                        <button type="submit" class="btn btn-success btn-block">Login</button>
                    </div>
                    <p><a href="{{ url('/password/reset') }}">Forgot Password?</a></p>
                </form>
            </div>
            <div class="dev-page-login-block__footer">
                © 2015 <strong>Aqvatarius</strong>. All rights reserved.
            </div>
        </div>
    </div>
@endsection