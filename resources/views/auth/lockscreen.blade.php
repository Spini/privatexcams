@extends('layouts.app')
@section('page-content')
    <div class="dev-page dev-page-lock-screen">

        <div class="dev-page-lock-screen-box">
            <div class="first-screen">
                {{--<div class="clock">--}}
                {{--<div id="dev-clock"></div>--}}
                {{--</div>--}}
                {{--<div class="date"></div>--}}
                <div class="lock-button">
                    <a href="#" class="btn btn-default"><i class="fa fa-lock"></i></a>
                </div>
                <div class="basement">
                    © 2015 <strong>Aqvatarius</strong>. All rights reserved.
                </div>
            </div>
            <div class="second-screen">
                <form action="{{ url('dashboard') }}" method="post">
                    <div class="photo">
                        <img src="img/backend/user_1.jpg">
                    </div>
                    <div class="title">Devin Stephens <span>UI/UX Designer</span></div>
                    <div class="elements">
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input class="form-control" type="password" name="password">
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="col-md-8 col-md-offset-2">
                                <button class="btn btn-danger btn-block">Unlock</button>
                            </div>
                        </div>
                    </div>
                    <div class="basement">
                        © 2015 <strong>Aqvatarius</strong>. All rights reserved.
                    </div>
                </form>
            </div>
        </div>

    </div>
    @push('plugins')
    {{--<script src="{{ asset('js/backend/raphael-min.js') }}"></script>--}}
    {{--<script src="{{ asset('js/backend/dev-clock.js') }}"></script>--}}
    @endpush
@endsection