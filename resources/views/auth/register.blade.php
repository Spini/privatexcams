@extends('layouts.app')
@section('page-content')
    <div class="dev-page dev-page-login dev-page-login-v2 dev-page-registration">

        <div class="dev-page-login-block">
            <a class="dev-page-login-block__logo">Intuitive</a>
            <div class="dev-page-login-block__form">
                <form role="form" action="{{ url('/register') }}" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name" name="first_name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Last Name" name="last_name">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group no-margin{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input id="name" type="text" class="form-control" placeholder="Username" name="username"
                                       value="{{ old('name') }}" required autofocus>
                            </div>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                            {{--<span class="help-block">Your profile - http://website.com/<strong>username</strong></span>--}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group margin-top-30{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="email" class="form-control" placeholder="Email" name="email"
                                       value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" type="password" class="form-control" placeholder="Password"
                                       name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input id="password-confirm" type="password" class="form-control" placeholder="Repeat Password"
                                       name="password_confirmation" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group no-border">
                        <button type="submit" class="btn btn-danger btn-block">Sign up</button>
                    </div>
                    <p class="text-center">OR</p>
                    <div class="form-group no-border">
                        <button class="btn btn-facebook btn-block">Facebook Connect</button>
                    </div>
                </form>
            </div>
            <div class="dev-page-login-block__footer">
                © 2015 <strong>Aqvatarius</strong>. All rights reserved.
            </div>
        </div>
    </div>
@endsection






