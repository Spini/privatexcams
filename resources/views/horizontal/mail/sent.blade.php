@extends('layouts.horizontal')
@section('content')
    <!-- email template -->
    <div class="dev-email">
        <!-- email navigation -->
        <div class="dev-email-navigation">
            <div class="dev-email-navigation-compose">
                <a href="{{ url('#') }}" class="btn btn-success btn-block"><i
                            class="fa fa-envelope"></i> Compose</a>
            </div>
            <ul>
                <li><a href="{{ url('#') }}"><i class="fa fa-inbox"></i> Inbox</a></li>
                <li><a href="{{ url('#') }}" class="active"><i class="fa fa-paper-plane"></i> Sent</a></li>
                <li><a href="{{ url('#') }}"><i class="fa fa-trash"></i> Trash</a></li>
            </ul>

            <div class="dev-email-navigation-labels">
                <div class="title">Labels</div>

                <a href="#"><i class="fa fa-circle text-success"></i> Friends</a>
                <a href="#"><i class="fa fa-circle text-warning"></i> Studios</a>
            </div>
        </div>
        <!-- ./email navigation -->

        <!-- list of messages -->
        <div class="dev-email-messages">

            <div class="dev-email-message-panel">
                <div class="checkbox">
                    <input type="checkbox" id="email_messages_checkall"/>
                    <label for="email_messages_checkall">&nbsp;</label>
                </div>
                <div class="btn-group pull-right">
                    <button class="btn btn-default"><i class="fa fa-warning"></i></button>
                    <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                </div>
            </div>

            <div class="dev-email-messages-list">

                <div class="dev-email-messages-list-item">
                    <div class="dev-email-messages-list-item-toolbar">
                        <div class="checkbox">
                            <input type="checkbox" id="email_message_1"/>
                            <label for="email_message_1">&nbsp;</label>
                        </div>
                        <div class="star">
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                    <div class="dev-email-messages-list-item-info">
                        <div class="name">Juan Saunders</div>
                        <div class="time">10:22 PM</div>
                        <div class="message">
                            An Internet email message consists of three components, the message envelope, the
                            message header, and the message body...
                        </div>
                    </div>
                </div>

                <div class="dev-email-messages-list-item">
                    <div class="dev-email-messages-list-item-toolbar">
                        <div class="checkbox">
                            <input type="checkbox" id="email_message_2"/>
                            <label for="email_message_2">&nbsp;</label>
                        </div>
                        <div class="star">
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                    <div class="dev-email-messages-list-item-info">
                        <div class="name">Michael Wade</div>
                        <div class="time">20th Jun</div>
                        <div class="message">
                            The message header contains control information, including, minimally, email address
                            and one or more recipient addresses....
                        </div>
                    </div>
                </div>

                <div class="dev-email-messages-list-item active">
                    <div class="dev-email-messages-list-item-toolbar">
                        <div class="checkbox">
                            <input type="checkbox" id="email_message_3"/>
                            <label for="email_message_3">&nbsp;</label>
                        </div>
                        <div class="star">
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                    <div class="dev-email-messages-list-item-info">
                        <div class="name">Carlos Gallagher</div>
                        <div class="time">19th Jun</div>
                        <div class="message">
                            Originally a text-only (ASCII) communications medium, Internet email was extended to
                            carry, e.g. text in other character sets...
                        </div>
                    </div>
                </div>

                <div class="dev-email-messages-list-item">
                    <div class="dev-email-messages-list-item-toolbar">
                        <div class="checkbox">
                            <input type="checkbox" id="email_message_4"/>
                            <label for="email_message_4">&nbsp;</label>
                        </div>
                        <div class="star">
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                    <div class="dev-email-messages-list-item-info">
                        <div class="name">Justin Grant</div>
                        <div class="time">19th Jun</div>
                        <div class="message">
                            An Internet email message consists of three components, the message envelope, the
                            message header, and the message body...
                        </div>
                    </div>
                </div>

                <div class="dev-email-messages-list-item">
                    <div class="dev-email-messages-list-item-toolbar">
                        <div class="checkbox">
                            <input type="checkbox" id="email_message_5"/>
                            <label for="email_message_5">&nbsp;</label>
                        </div>
                        <div class="star">
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                    <div class="dev-email-messages-list-item-info">
                        <div class="name">Jeffrey Pham</div>
                        <div class="time">18th Jun</div>
                        <div class="message">
                            The message header contains control information, including, minimally, email address
                            and one or more recipient addresses....
                        </div>
                    </div>
                </div>

                <div class="dev-email-messages-list-item">
                    <div class="dev-email-messages-list-item-toolbar">
                        <div class="checkbox">
                            <input type="checkbox" id="email_message_6"/>
                            <label for="email_message_6">&nbsp;</label>
                        </div>
                        <div class="star">
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                    <div class="dev-email-messages-list-item-info">
                        <div class="name">Tyrone Becker</div>
                        <div class="time">18th Jun</div>
                        <div class="message">
                            Originally a text-only (ASCII) communications medium, Internet email was extended to
                            carry, e.g. text in other character sets...
                        </div>
                    </div>
                </div>

                <div class="dev-email-messages-list-item">
                    <div class="dev-email-messages-list-item-toolbar">
                        <div class="checkbox">
                            <input type="checkbox" id="email_message_7"/>
                            <label for="email_message_7">&nbsp;</label>
                        </div>
                        <div class="star">
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                    <div class="dev-email-messages-list-item-info">
                        <div class="name">Shanice Pierce</div>
                        <div class="time">18th Jun</div>
                        <div class="message">
                            An Internet email message consists of three components, the message envelope, the
                            message header, and the message body...
                        </div>
                    </div>
                </div>

                <div class="dev-email-messages-list-item">
                    <div class="dev-email-messages-list-item-toolbar">
                        <div class="checkbox">
                            <input type="checkbox" id="email_message_8"/>
                            <label for="email_message_8">&nbsp;</label>
                        </div>
                        <div class="star">
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                    <div class="dev-email-messages-list-item-info">
                        <div class="name">Francisco Robbins</div>
                        <div class="time">18th Jun</div>
                        <div class="message">
                            The message header contains control information, including, minimally, email address
                            and one or more recipient addresses....
                        </div>
                    </div>
                </div>

                <div class="dev-email-messages-list-item">
                    <div class="dev-email-messages-list-item-toolbar">
                        <div class="checkbox">
                            <input type="checkbox" id="email_message_9"/>
                            <label for="email_message_9">&nbsp;</label>
                        </div>
                        <div class="star">
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                    <div class="dev-email-messages-list-item-info">
                        <div class="name">Brooklyn Ball</div>
                        <div class="time">18th Jun</div>
                        <div class="message">
                            Originally a text-only (ASCII) communications medium, Internet email was extended to
                            carry, e.g. text in other character sets...
                        </div>
                    </div>
                </div>

                <div class="dev-email-messages-list-item">
                    <div class="dev-email-messages-list-item-toolbar">
                        <div class="checkbox">
                            <input type="checkbox" id="email_message_10"/>
                            <label for="email_message_10">&nbsp;</label>
                        </div>
                        <div class="star">
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                    <div class="dev-email-messages-list-item-info">
                        <div class="name">Juan Saunders</div>
                        <div class="time">17th Jun</div>
                        <div class="message">
                            An Internet email message consists of three components, the message envelope, the
                            message header, and the message body...
                        </div>
                    </div>
                </div>

                <div class="dev-email-messages-list-item">
                    <div class="dev-email-messages-list-item-toolbar">
                        <div class="checkbox">
                            <input type="checkbox" id="email_message_11"/>
                            <label for="email_message_11">&nbsp;</label>
                        </div>
                        <div class="star">
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                    <div class="dev-email-messages-list-item-info">
                        <div class="name">Michael Wade</div>
                        <div class="time">17th Jun</div>
                        <div class="message">
                            The message header contains control information, including, minimally, email address
                            and one or more recipient addresses....
                        </div>
                    </div>
                </div>

                <div class="dev-email-messages-list-item">
                    <div class="dev-email-messages-list-item-toolbar">
                        <div class="checkbox">
                            <input type="checkbox" id="email_message_12"/>
                            <label for="email_message_12">&nbsp;</label>
                        </div>
                        <div class="star">
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                    <div class="dev-email-messages-list-item-info">
                        <div class="name">Carlos Gallagher</div>
                        <div class="time">17th Jun</div>
                        <div class="message">
                            Originally a text-only (ASCII) communications medium, Internet email was extended to
                            carry, e.g. text in other character sets...
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- ./list of messages -->

        <!-- email message -->
        <div class="dev-email-message">

            <div class="dev-email-message-panel">
                <div class="btn-group">
                    <button class="btn btn-default"><i class="fa fa-mail-reply"></i></button>
                    <button class="btn btn-default"><i class="fa fa-mail-reply-all"></i></button>
                </div>
                <button class="btn btn-default"><i class="fa fa-star-o"></i></button>
                <div class="btn-group">
                    <button class="btn btn-default"><i class="fa fa-warning"></i></button>
                    <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                </div>
            </div>

            <div class="dev-email-message-title">Network-based email was initially exchanged</div>
            <div class="dev-email-message-from">
                <img src="img/backend/user_1.jpg"> Carlos Gallagher
            </div>
            <div class="dev-email-message-date">
                17:55, 19 June 2015
            </div>
            <div class="dev-email-message-text">
                <p>Hi John,</p>
                <p>An Internet email message consists of three components, the message envelope, the message
                    header, and the message body. The message header contains control information, including,
                    minimally, an originator's email address and one or more recipient addresses. Usually
                    descriptive information is also added, such as a subject header field and a message
                    submission date/time stamp.</p>
                <p>Email is an information and communications technology. It uses technology to communicate a
                    digital message over the Internet. Users use email differently, based on how they think
                    about it. There are many software platforms available to send and receive. Popular email
                    platforms include Gmail, Hotmail, Yahoo! Mail, Outlook, and many others.</p>
                <p>&nbsp;</p>
                <p><strong>Thank you...</strong><br>Carlos Gallagher</p>
            </div>

            <div class="dev-email-message-attachment">
                <strong>Attachments:</strong><br>
                <a href="#">s-7552-22.doc</a>, <a href="#">my-photo-12(1).jpg</a>
            </div>

            <div class="dev-email-message-form">
                <div class="form-group">
                    <textarea class="form-control" placeholder="Post a quick reply" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-default"><i class="fa fa-paperclip"></i></button>
                    <button class="btn btn-default"><i class="fa fa-photo"></i></button>
                    <button class="btn btn-danger pull-right">Send</button>
                </div>
            </div>
        </div>
        <!-- ./email message -->
    </div>
    <!-- ./email template -->
@endsection