@extends('layouts.app')
@section('content')
    <div id="container">
        <div class="item w2 red">
            <img src="{{ asset('img/image1.jpg') }}" alt="">
            <div class="description">
                <h4>welcome</h4>
                <h1>Ampersan <span>theme</span> is great</h1>
                <p>Nec habitasse. Magnis lorem in cum. Et? Rhoncus sociis magna. Massa ac elementum vel, odio et natoque
                    porta nisi!</p>
                <a class="readMore">Find out More</a>
            </div>
        </div>

        <div class="item">
            <ul class="grid cs-style-3">
                <li>
                    <figure>
                        <img src="{{ asset('img/image2.jpg') }}" alt="">

                        <figcaption>
                            <a href="{{ asset('img/image2.jpg') }}" data-rel="prettyPhoto">
                                <h3>Image Title</h3>
                                <div class="readMore"></div>
                            </a>
                        </figcaption>

                    </figure>
                </li>
            </ul>
        </div>

        <div class="item">
            <ul class="grid cs-style-3">
                <li>
                    <figure>
                        <img src="{{ asset('img/image3.jpg') }}" alt="">

                        <figcaption>
                            <a href="{{ asset('img/image3.jpg') }}" data-rel="prettyPhoto">
                                <h3>Image Title</h3>
                                <div class="readMore"></div>
                            </a>
                        </figcaption>

                    </figure>
                </li>
            </ul>
        </div>

        <div class="item w2 red shadow">
            <img src="{{ asset('img/image6.jpg') }}" alt="">
            <div class="descriptionCenter">
                <a class="play" href="http://vimeo.com/8245346" data-rel="prettyPhoto" title=""></a>
                <h1>Movie <span>title</span></h1>
                <p>Nec habitasse. Magnis lorem in cum. Et? Rhoncus sociis magna. Massa ac elementum vel, odio et natoque
                    porta nisi!</p>
            </div>
        </div>

        <div class="item">
            <ul class="grid cs-style-3">
                <li>
                    <figure>
                        <img src="{{ asset('img/image4.jpg') }}" alt="">

                        <figcaption>
                            <a href="{{ asset('img/image4.jpg') }}" data-rel="prettyPhoto">
                                <h3>Image Title</h3>
                                <div class="readMore"></div>
                            </a>
                        </figcaption>

                    </figure>
                </li>
            </ul>
        </div>
        <div class="item">
            <ul class="grid cs-style-3">
                <li>
                    <figure>
                        <img src="{{ asset('img/image5.jpg') }}" alt="">

                        <figcaption>
                            <a href="{{ asset('img/image5.jpg') }}" data-rel="prettyPhoto">
                                <h3>Image Title</h3>
                                <div class="readMore"></div>
                            </a>
                        </figcaption>

                    </figure>
                </li>
            </ul>
        </div>


    </div>
@endsection