@extends('layouts.default')
@section('content')
    <div id="page-container">
        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Header -->
            <div class="content bg-gray-lighter">
                <div class="row items-push">
                    <div class="col-sm-8">
                        <h1 class="page-heading">
                            Schedule
                            <small>Set your schedule here!
                            </small>
                        </h1>
                    </div>
                </div>
            </div>
            <!-- END Page Header -->

            <!-- Page Content -->
            <div class="content bg-white">
                <!-- Calendar and Events functionality (initialized in js/pages/base_comp_calendar.js), for more info and examples you can check out http://fullcalendar.io/ -->
                <div class="row items-push">
                    <div class="col-md-4 col-md-push-8 col-lg-2 col-lg-push-10">
                        <!-- Add Event Form -->
                        <form class="js-form-add-event push-30" action=""
                              method="post">
                            <div class="input-group">
                                <input class="js-add-event form-control" type="text"
                                       placeholder="Add event..">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!-- END Add Event Form -->

                        <!-- Event List -->
                        <ul class="js-events list list-events">
                            <li style="background-color: #fac5a5;">Day off</li>
                            <li style="background-color: #b5d0eb;">Brake</li>
                            {{--<li style="background-color: #faeab9;">Gym</li>--}}
                            <li style="background-color: #c8e2b3;">Work</li>
                        </ul>
                        <div class="text-center text-muted">
                            <small><em><i class="fa fa-arrows"></i> Drag and drop events on the
                                    calendar</em></small>
                        </div>
                        <!-- END Event List -->
                        <br>
                        <div class="row text-center text-muted">
                            <button class="btn btn-primary">Save schedule</button>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-pull-4 col-lg-10 col-lg-pull-2">
                        <!-- Calendar Container -->
                        <div class="js-calendar"></div>
                    </div>
                </div>
                <!-- END Calendar -->
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
    </div>
    <div class="wrapper">
        <div class="page-subtitle">
            <h2>View schedule <span> -24/04/2017</span></h2>
            <div class="col-md-2">
                <input type="text" class="form-control datepicker">
            </div>
            <div class="col-md-3" style="float: right">
                <form id="dev-faq-form">
                    <div class="input-group">
                        <input type="text" id="dev-faq-keyword" placeholder="Search..."
                               class="form-control">
                        <div class="input-group-btn">
                            <button id="dev-faq-search" class="btn btn-default"><i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                    <p id="dev-faq-search-result"></p></form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover text-center">
                <tr>
                    <th>Model</th>
                    <th>Raiting</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th>11</th>
                    <th class="end-day">12</th>

                    <th>13</th>
                    <th>14</th>
                    <th>15</th>
                    <th>16</th>
                    <th>17</th>
                    <th>18</th>
                    <th>19</th>
                    <th>20</th>
                    <th>21</th>
                    <th>22</th>
                    <th>23</th>
                    <th>24</th>
                    <th>Status</th>
                </tr>
                <tr>
                    <td>
                        <div class="list-contacts list-contacts-inline" style="width: 160px;">
                            <a href="#" class="list-contacts-item contact-online">
                                <img src="{{ asset('img/backend/user_2.jpg') }}"> Devin Stephens
                            </a>
                        </div>
                    </td>
                    <td>4.4/5</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td class="end-day">0</td>

                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td><span class="label label-success">Online</span></td>
                </tr>
                <tr>
                    <td>
                        <div class="list-contacts list-contacts-inline" style="width: 160px;">
                            <a href="#" class="list-contacts-item contact-away">
                                <img src="{{ asset('img/backend/user_3.jpg') }}"> Marissa George
                            </a>
                        </div>
                    </td>
                    <td>5/5</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td class="end-day">0</td>

                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td><span class="label label-warning">Private</span></td>
                </tr>
                <tr>
                    <td>
                        <div class="list-contacts list-contacts-inline" style="width: 160px;">
                            <a href="#" class="list-contacts-item contact-offline">
                                <img src="{{ asset('img/backend/user_1.jpg') }}"> John Doe
                            </a>
                        </div>
                    </td>
                    <td>2/5</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td class="end-day">0</td>

                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td><span class="label label-danger">Offline</span></td>
                </tr>
                <tr>
                    <td>
                        <div class="list-contacts list-contacts-inline" style="width: 160px;">
                            <a href="#" class="list-contacts-item contact-online">
                                <img src="{{ asset('img/backend/user_4.jpg') }}"> Karen Spencer
                            </a>
                        </div>
                    </td>
                    <td>1/5</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td class="end-day">0</td>

                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td><span class="label label-success">Online</span></td>
                </tr>
            </table>
        </div>
    </div>
@endsection
@push('css')
<link rel="stylesheet" href="css/backend/fullcalendar.min.css">
<link rel="stylesheet" id="css-main" href="css/backend/oneui.min.css">
@endpush

@push('plugins')
<script src="{{ asset('js/backend/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('js/backend/spectrum.js') }}"></script>
<script src="{{ asset('js/backend/jquery.tagsinput.min.js') }}"></script>

<script src="{{ asset('js/backend/app.js') }}"></script>
<script src="{{ asset('js/backend/fullcalendar.min.js') }}"></script>
<script src="{{ asset('js/backend/gcal.min.js') }}"></script>

<script src="{{ asset('js/backend/base_comp_calendar.js') }}"></script>
@endpush