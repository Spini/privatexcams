@extends('layouts.default')
@section('content')
    <div class="wrapper wrapper-white">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="page-subtitle page-subtitle-centralized">
                    <h3>Search in FAQ</h3>
                    <p>Use form below to find answers on your questions</p>
                </div>

                <form id="dev-faq-form">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" id="dev-faq-keyword" placeholder="Search...">
                            <div class="input-group-btn">
                                <button class="btn btn-default" id="dev-faq-search"><i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <p id="dev-faq-search-result"></p>
                </form>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <button class="btn btn-default btn-icon" id="dev-faq-open"><i class="fa fa-angle-down"></i> Open
                        All
                    </button>
                    <button class="btn btn-default btn-icon" id="dev-faq-close"><i class="fa fa-angle-up"></i> Close
                        All
                    </button>
                </div>
                <div class="pull-right">
                    <button class="btn btn-default" id="dev-faq-remove-highlights">Remove Highlights</button>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper">
        <div class="row">
            <div class="col-md-12">

                <div class="faq">
                    <h2><i class="fa fa-cubes"></i> General Questions</h2>

                    <div class="faq-item">
                        <div class="faq-title"><span class="fa fa-angle-down"></span> How to aliquam at ipsum
                            sapien?
                        </div>
                        <div class="faq-text">
                            <h5>Aliquam at ipsum sapien</h5>
                            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
                                himenaeos. Donec adipiscing vehicula tortor dapibus adipiscing.</p>
                            <p>Nullam quis quam massa. Donec vitae metus tortor. Vestibulum vel diam orci. Etiam
                                sollicitudin venenatis justo ut posuere. Etiam facilisis est ut ligula ornare
                                accumsan. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
                                inceptos himenaeos.</p>
                        </div>
                    </div>

                    <div class="faq-item">
                        <div class="faq-title"><span class="fa fa-angle-down"></span> Nunc pellentesque sagittis
                            pulvinar?
                        </div>
                        <div class="faq-text">
                            <h5>Pellentesque sagittis pulvinar</h5>
                            <p>Nunc pellentesque sagittis pulvinar. Donec et bibendum dolor. Praesent commodo
                                facilisis dui, vitae euismod ipsum aliquam gravida. Nulla aliquet fringilla velit
                                sit amet dignissim. Sed justo ex, mattis sed venenatis sit amet, varius at urna.
                                Donec erat nunc, tempus id tortor vel, consequat pulvinar nisl. Donec sed felis in
                                erat malesuada tincidunt pulvinar in lorem.</p>
                            <p>Etiam rutrum, leo ut molestie hendrerit, quam elit semper nunc, eget ullamcorper sem
                                ligula a nisl. Phasellus aliquam efficitur elit sed ullamcorper. Quisque porttitor
                                ac turpis quis sodales.</p>
                            <h5>Hendrerit luctus</h5>
                            <p>Nulla dapibus turpis ornare est hendrerit luctus. Nam id turpis sapien. Quisque non
                                fermentum nisl. In sagittis nibh non dolor condimentum sodales.</p>
                        </div>
                    </div>

                    <div class="faq-item">
                        <div class="faq-title"><span class="fa fa-angle-down"></span> Donec libero nisl, hendrerit
                            vel tempus at?
                        </div>
                        <div class="faq-text">
                            <h5>Cras ac odio faucibus tortor pretium</h5>
                            <p>Cras ac odio faucibus tortor pretium tristique in id nisl. Donec libero nisl,
                                hendrerit vel tempus at, posuere vel urna. Nam sed consectetur lectus. Sed sit amet
                                risus dolor. Duis accumsan lorem ac quam egestas pretium.</p>
                            <p>Curabitur finibus nisl ac aliquet mattis. Aliquam convallis bibendum lorem sed
                                lobortis. Cras aliquam urna sed luctus tincidunt.</p>
                            <h5>Nulla ullamcorper</h5>
                            <p>In diam turpis, tristique nec cursus in, blandit vel elit. Nulla ullamcorper, ex in
                                ultrices fringilla, nisi sapien hendrerit dolor, in suscipit mauris turpis id
                                erat.</p>
                            <p>Nunc facilisis odio vitae eros rutrum, eget rutrum nulla rhoncus. Etiam laoreet
                                pretium ex ut gravida. In venenatis turpis sit amet volutpat bibendum.</p>
                        </div>
                    </div>

                    <div class="faq-item">
                        <div class="faq-title"><span class="fa fa-angle-down"></span> Vestibulum vel diam orci?
                        </div>
                        <div class="faq-text">
                            <h5>Aliquam at ipsum sapien</h5>
                            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
                                himenaeos. Donec adipiscing vehicula tortor dapibus adipiscing.</p>
                            <p>Nullam quis quam massa. Donec vitae metus tortor. Vestibulum vel diam orci. Etiam
                                sollicitudin venenatis justo ut posuere. Etiam facilisis est ut ligula ornare
                                accumsan. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
                                inceptos himenaeos.</p>
                        </div>
                    </div>

                    <div class="faq-item">
                        <div class="faq-title"><span class="fa fa-angle-down"></span> Nam id turpis sapien?</div>
                        <div class="faq-text">
                            <h5>Pellentesque sagittis pulvinar</h5>
                            <p>Nunc pellentesque sagittis pulvinar. Donec et bibendum dolor. Praesent commodo
                                facilisis dui, vitae euismod ipsum aliquam gravida. Nulla aliquet fringilla velit
                                sit amet dignissim. Sed justo ex, mattis sed venenatis sit amet, varius at urna.
                                Donec erat nunc, tempus id tortor vel, consequat pulvinar nisl. Donec sed felis in
                                erat malesuada tincidunt pulvinar in lorem.</p>
                            <p>Etiam rutrum, leo ut molestie hendrerit, quam elit semper nunc, eget ullamcorper sem
                                ligula a nisl. Phasellus aliquam efficitur elit sed ullamcorper. Quisque porttitor
                                ac turpis quis sodales.</p>
                            <h5>Hendrerit luctus</h5>
                            <p>Nulla dapibus turpis ornare est hendrerit luctus. Nam id turpis sapien. Quisque non
                                fermentum nisl. In sagittis nibh non dolor condimentum sodales.</p>
                        </div>
                    </div>

                    <div class="faq-item">
                        <div class="faq-title"><span class="fa fa-angle-down"></span> Nulla ullamcorper, ex in
                            ultrices fringilla?
                        </div>
                        <div class="faq-text">
                            <h5>Cras ac odio faucibus tortor pretium</h5>
                            <p>Cras ac odio faucibus tortor pretium tristique in id nisl. Donec libero nisl,
                                hendrerit vel tempus at, posuere vel urna. Nam sed consectetur lectus. Sed sit amet
                                risus dolor. Duis accumsan lorem ac quam egestas pretium.</p>
                            <p>Curabitur finibus nisl ac aliquet mattis. Aliquam convallis bibendum lorem sed
                                lobortis. Cras aliquam urna sed luctus tincidunt.</p>
                            <h5>Nulla ullamcorper</h5>
                            <p>In diam turpis, tristique nec cursus in, blandit vel elit. Nulla ullamcorper, ex in
                                ultrices fringilla, nisi sapien hendrerit dolor, in suscipit mauris turpis id
                                erat.</p>
                            <p>Nunc facilisis odio vitae eros rutrum, eget rutrum nulla rhoncus. Etiam laoreet
                                pretium ex ut gravida. In venenatis turpis sit amet volutpat bibendum.</p>
                        </div>
                    </div>


                    <h2><i class="fa fa-money"></i> Payment</h2>
                    <div class="faq-item">
                        <div class="faq-title"><span class="fa fa-angle-down"></span> Cras at turpis vestibulum
                            mauris gravida commodo?
                        </div>
                        <div class="faq-text">
                            <h5>Aliquam at ipsum sapien</h5>
                            <p>Maecenas risus sapien, sollicitudin quis nisl vehicula, sagittis venenatis elit. Cras
                                at turpis vestibulum mauris gravida commodo. Fusce tellus metus, eleifend vel
                                ultrices quis, fermentum ut justo. Ut hendrerit ante sed rutrum sagittis. Nam ac
                                nulla posuere, mattis risus nec, sagittis purus. Praesent in justo rhoncus, molestie
                                velit laoreet, viverra sem.</p>
                            <p>Sed sit amet lacus sem. Sed vel fermentum mi, sit amet hendrerit purus. Duis nec
                                posuere dolor. Fusce sed faucibus turpis, a cursus nunc.</p>
                        </div>
                    </div>

                    <div class="faq-item">
                        <div class="faq-title"><span class="fa fa-angle-down"></span> Nunc pellentesque sagittis
                            pulvinar?
                        </div>
                        <div class="faq-text">
                            <h5>Pellentesque sagittis pulvinar</h5>
                            <p>Nunc pellentesque sagittis pulvinar. Donec et bibendum dolor. Praesent commodo
                                facilisis dui, vitae euismod ipsum aliquam gravida. Nulla aliquet fringilla velit
                                sit amet dignissim. Sed justo ex, mattis sed venenatis sit amet, varius at urna.
                                Donec erat nunc, tempus id tortor vel, consequat pulvinar nisl. Donec sed felis in
                                erat malesuada tincidunt pulvinar in lorem.</p>
                            <p>Etiam rutrum, leo ut molestie hendrerit, quam elit semper nunc, eget ullamcorper sem
                                ligula a nisl. Phasellus aliquam efficitur elit sed ullamcorper. Quisque porttitor
                                ac turpis quis sodales.</p>
                            <h5>Hendrerit luctus</h5>
                            <p>Nulla dapibus turpis ornare est hendrerit luctus. Nam id turpis sapien. Quisque non
                                fermentum nisl. In sagittis nibh non dolor condimentum sodales.</p>
                        </div>
                    </div>

                    <div class="faq-item">
                        <div class="faq-title"><span class="fa fa-angle-down"></span> Donec libero nisl, hendrerit
                            vel tempus at?
                        </div>
                        <div class="faq-text">
                            <h5>Cras ac odio faucibus tortor pretium</h5>
                            <p>Cras ac odio faucibus tortor pretium tristique in id nisl. Donec libero nisl,
                                hendrerit vel tempus at, posuere vel urna. Nam sed consectetur lectus. Sed sit amet
                                risus dolor. Duis accumsan lorem ac quam egestas pretium.</p>
                            <p>Curabitur finibus nisl ac aliquet mattis. Aliquam convallis bibendum lorem sed
                                lobortis. Cras aliquam urna sed luctus tincidunt.</p>
                            <h5>Nulla ullamcorper</h5>
                            <p>In diam turpis, tristique nec cursus in, blandit vel elit. Nulla ullamcorper, ex in
                                ultrices fringilla, nisi sapien hendrerit dolor, in suscipit mauris turpis id
                                erat.</p>
                            <p>Nunc facilisis odio vitae eros rutrum, eget rutrum nulla rhoncus. Etiam laoreet
                                pretium ex ut gravida. In venenatis turpis sit amet volutpat bibendum.</p>
                        </div>
                    </div>

                    <div class="faq-item">
                        <div class="faq-title"><span class="fa fa-angle-down"></span> Vestibulum vel diam orci?
                        </div>
                        <div class="faq-text">
                            <h5>Aliquam at ipsum sapien</h5>
                            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
                                himenaeos. Donec adipiscing vehicula tortor dapibus adipiscing.</p>
                            <p>Nullam quis quam massa. Donec vitae metus tortor. Vestibulum vel diam orci. Etiam
                                sollicitudin venenatis justo ut posuere. Etiam facilisis est ut ligula ornare
                                accumsan. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
                                inceptos himenaeos.</p>
                        </div>
                    </div>

                    <div class="faq-item">
                        <div class="faq-title"><span class="fa fa-angle-down"></span> Nam id turpis sapien?</div>
                        <div class="faq-text">
                            <h5>Pellentesque sagittis pulvinar</h5>
                            <p>Nunc pellentesque sagittis pulvinar. Donec et bibendum dolor. Praesent commodo
                                facilisis dui, vitae euismod ipsum aliquam gravida. Nulla aliquet fringilla velit
                                sit amet dignissim. Sed justo ex, mattis sed venenatis sit amet, varius at urna.
                                Donec erat nunc, tempus id tortor vel, consequat pulvinar nisl. Donec sed felis in
                                erat malesuada tincidunt pulvinar in lorem.</p>
                            <p>Etiam rutrum, leo ut molestie hendrerit, quam elit semper nunc, eget ullamcorper sem
                                ligula a nisl. Phasellus aliquam efficitur elit sed ullamcorper. Quisque porttitor
                                ac turpis quis sodales.</p>
                            <h5>Hendrerit luctus</h5>
                            <p>Nulla dapibus turpis ornare est hendrerit luctus. Nam id turpis sapien. Quisque non
                                fermentum nisl. In sagittis nibh non dolor condimentum sodales.</p>
                        </div>
                    </div>

                    <div class="faq-item">
                        <div class="faq-title"><span class="fa fa-angle-down"></span> Nulla ullamcorper, ex in
                            ultrices fringilla?
                        </div>
                        <div class="faq-text">
                            <h5>Cras ac odio faucibus tortor pretium</h5>
                            <p>Cras ac odio faucibus tortor pretium tristique in id nisl. Donec libero nisl,
                                hendrerit vel tempus at, posuere vel urna. Nam sed consectetur lectus. Sed sit amet
                                risus dolor. Duis accumsan lorem ac quam egestas pretium.</p>
                            <p>Curabitur finibus nisl ac aliquet mattis. Aliquam convallis bibendum lorem sed
                                lobortis. Cras aliquam urna sed luctus tincidunt.</p>
                            <h5>Nulla ullamcorper</h5>
                            <p>In diam turpis, tristique nec cursus in, blandit vel elit. Nulla ullamcorper, ex in
                                ultrices fringilla, nisi sapien hendrerit dolor, in suscipit mauris turpis id
                                erat.</p>
                            <p>Nunc facilisis odio vitae eros rutrum, eget rutrum nulla rhoncus. Etiam laoreet
                                pretium ex ut gravida. In venenatis turpis sit amet volutpat bibendum.</p>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection