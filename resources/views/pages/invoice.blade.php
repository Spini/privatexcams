@extends('layouts.default')
@section('content')
    <div class="dev-invoice">

        <div class="dev-invoice-title">
            <div class="dev-invoice-title-logo">
                <img src="img/backend/logo-invoice.png">
            </div>
            <div class="dev-invoice-title-info">
                Invoice #1,033<br>
                <span>June 21, 2015</span>
            </div>
        </div>
        <table class="dev-invoice-table">
            <tbody>
            <tr>
                <th width="10%">Hours</th>
                <th width="70%">Task Description</th>
                <th width="10%">Rate</th>
                <th width="10%">Total</th>
            </tr>
            <tr>
                <td>02.50</td>
                <td><strong>Dashboard: Sketching</strong></td>
                <td class="sub-color">$35</td>
                <td><strong>$87.5</strong></td>
            </tr>
            <tr>
                <td>05.10</td>
                <td><strong>Dashboard: Markup/Coding</strong></td>
                <td class="sub-color">$35</td>
                <td><strong>$178.5</strong></td>
            </tr>
            <tr>
                <td>01.50</td>
                <td><strong>Contact: Sketching</strong></td>
                <td class="sub-color">$35</td>
                <td><strong>$52.5</strong></td>
            </tr>
            <tr>
                <td>02.00</td>
                <td><strong>Contact: Markup/Coding</strong></td>
                <td class="sub-color">$35</td>
                <td><strong>$70</strong></td>
            </tr>
            </tbody>
        </table>

        <div class="dev-invoice-total">
            <div class="dev-invoice-total-row">11.40</div>
        </div>
        <div class="dev-invoice-total pull-right">
            <div class="dev-invoice-total-row"><span>Subtotal:</span> $388,5</div>
            <div class="dev-invoice-total-row"><span>Taxes/Fees(20%):</span> $77.7</div>
            <div class="dev-invoice-total-row total"><span>Total to be paid:</span> $466.2</div>
        </div>
        <div class="dev-invoice-notes">
            <strong>Notes:</strong> Paypal Address: ppaddress@domain.com
        </div>
    </div>
@endsection