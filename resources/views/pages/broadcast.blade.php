@extends('layouts.default')
@section('content')
    <div class="row row-condensed">
        <div class="col-lg-9 col-md-6">

            <div class="wrapper">
                <video width="100%" autoplay>
                    <source src="{{ asset('img/backend/Sinner.mp4') }}" type="video/mp4">
                </video>
                <button class="bio btn btn-primary"><a class="live" style="text-decoration-color: white" href="#">Give
                        a gift</a></button>
                <button class="private btn btn-danger"><a class="live" style="text-decoration-color: white"
                                                          href="#">Start
                        Private Show</a></button>
                <button class="credits btn btn-warning"><a class="live" style="text-decoration-color: white"
                                                           href="#"><i class="fa   fa-plus-square-o"></i> Give
                        credits</a></button>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="widget-tabbed margin-top-30">
                <ul class="widget-tabs widget-tabs-three">
                    <li class="active"><a href="#tasks">Tasks</a></li>
                    <li><a href="#activity">Activity</a></li>
                </ul>
                <div class="widget-tab list-tasks active" id="tasks">

                    <div class="list-tasks-item primary">
                        <div class="checkbox">
                            <input type="checkbox" id="task_1">
                            <label for="task_1"></label>
                        </div>
                        <div class="task">
                            <a href="#">The term is commonly used as a synonym for computers and computer
                                networks</a>
                            <div class="user"><i class="fa fa-user"></i> <a href="#">John Doe</a></div>
                            <div class="date">13:30</div>
                        </div>
                    </div>

                    <div class="list-tasks-item danger">
                        <div class="checkbox">
                            <input type="checkbox" id="task_2">
                            <label for="task_2"></label>
                        </div>
                        <div class="task">
                            <a href="#">Humans have been storing, retrieving, manipulating and communicating
                                information</a>
                            <div class="user"><i class="fa fa-user"></i> <a href="#">Devin Stephens</a>
                            </div>
                            <div class="date">11:22</div>
                        </div>
                    </div>

                    <div class="list-tasks-item danger">
                        <div class="checkbox">
                            <input type="checkbox" id="task_3">
                            <label for="task_3"></label>
                        </div>
                        <div class="task">
                            <a href="#">Based on the storage and processing technologies employed</a>
                            <div class="user"><i class="fa fa-user"></i> <a href="#">Kaitlynn Bowen</a>
                            </div>
                            <div class="date">10:59</div>
                        </div>
                    </div>
                </div>
                <div class="widget-tab" id="activity">

                    <ul class="timeline-simple">
                        <li class="success">
                            <span class="timeline-simple-date">12 min ago</span>
                            The standard Lorem Ipsum passage, used since the 1500s
                        </li>
                        <li class="success">
                            <span class="timeline-simple-date">2h ago</span>
                            Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
                        </li>
                        <li class="info">
                            <span class="timeline-simple-date">15h ago</span>
                            1914 translation by H. Rackham
                        </li>
                        <li class="info">
                            <span class="timeline-simple-date">19h ago</span>
                            Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection