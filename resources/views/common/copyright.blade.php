<div class="copyright">
    <div class="pull-left">
        &copy; 2015 <strong>Aqvatarius</strong>. All rights reserved.
    </div>
    <div class="pull-right">
        <a href="{{ url('#') }}">Terms of use</a> / <a href="{{ url('#') }}">Privacy Policy</a>
    </div>
</div>
