<!-- page sidebar -->
<div class="dev-page-sidebar">
    @php($user = auth()->user())
    <div class="profile profile-transparent">
        <div class="profile-image">
            <a href="{{ url('show') }}">
                <img src="{{ Voyager::image($user->avatar, asset('img/backend/user_7.jpg')) }}">
            </a>
            <div class="profile-badges">
                <a href="#" class="profile-badges-left"><i class="fa fa-trophy"></i> 243</a>
                <a href="{{ url('friends') }}" class="profile-badges-right"><i class="fa fa-users"></i> 1,971</a>
            </div>
            <div class="profile-status online"></div>
        </div>
        <div class="profile-info">
            <h4>{{ $user->name }}</h4>
            <span>{{ $user->role->display_name }}</span>
        </div>
    </div>

    <ul class="dev-page-navigation">
        <li class="title">User</li>
        @php($sidebar_menu = App\Menu::sideBar())
        @foreach($sidebar_menu as $item)
            <li>
                <a href="{{ $item['url'] }}" class="dev-page-navigation {{ App\Menu::isUrl( $item['url'] )}}">
                    <i class="{{ $item['icon'] }}"></i>
                    {!! $item['display'] !!}
                </a>
            </li>
        @endforeach
    </ul>

</div>
<!-- ./page sidebar -->