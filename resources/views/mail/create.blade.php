@extends('layouts.default')
@section('content')
    <!-- email template -->
    <div class="dev-email">
        <!-- email navigation -->
        <div class="dev-email-navigation">
            <div class="dev-email-navigation-compose hide-mobile">
                <a href="{{ url('compose') }}" class="btn btn-success btn-block"><i
                            class="fa fa-envelope"></i> Compose</a>
            </div>
            <ul>
                <li><a href="{{ url('inbox') }}"><i class="fa fa-inbox"></i> Inbox</a></li>
                <li><a href="{{ url('sent') }}"><i class="fa fa-paper-plane"></i> Sent</a></li>
                <li><a href="{{ url('trash') }}"><i class="fa fa-trash"></i> Trash</a></li>
            </ul>

            <div class="dev-email-navigation-labels">
                <div class="title">Labels</div>

                <a href="#"><i class="fa fa-circle text-success"></i> Friends</a>
                <a href="#"><i class="fa fa-circle text-warning"></i> Studios</a>

            </div>
        </div>
        <!-- ./email navigation -->

        <!-- compose message -->
        <div class="dev-email-compose">
            <div class="dev-email-message-panel">
                <button class="btn btn-default"><i class="fa fa-floppy-o"></i> Save</button>
                <button class="btn btn-danger pull-right"><i class="fa fa-paper-plane"></i> Send</button>
            </div>
            <div class="dev-email-compose-form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>To:</label>
                            <input type="text" class="tags" name="email_to" value="johndoe@example.com">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Cc:</label>
                            <input type="text" class="tags" name="email_cc">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Subject:</label>
                            <input type="text" class="form-control" name="email_subject">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Attachment:</label><br>
                            <input type="file" class="file" title="Choose file" name="email_attachment">
                        </div>
                    </div>
                </div>
                <div class="summernote">
                    <p><strong>Hi John</strong>,</p>
                    <p>An Internet email message consists of three components, the message envelope, the message
                        header, and the message body. The message header contains control information,
                        including, minimally, an originator's email address and one or more recipient addresses.
                        Usually descriptive information is also added, such as a subject header field and a
                        message submission date/time stamp.</p>
                    <p>Email is an information and <a href="#">communications technology</a>. It uses technology
                        to communicate a digital message over the Internet. Users use email differently, based
                        on how they think about it. There are many software platforms available to send and
                        receive. Popular email platforms include Gmail, Hotmail, Yahoo! Mail, Outlook, and many
                        others.</p>
                    <p>&nbsp;</p>
                    <p><strong>Thank you...</strong><br>Carlos Gallagher</p>
                </div>
                <div class="form-group">
                    <div class="checkbox pull-left">
                        <input type="checkbox" id="send_copy">
                        <label for="send_copy">Send me a copy</label>
                    </div>
                </div>
            </div>

        </div>
        <!-- ./compose message -->

    </div>
    <!-- ./email template -->
@endsection