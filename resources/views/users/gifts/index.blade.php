@extends('layouts.default')
@section('content')
    <div class="page-profile" style="background-image: url(img/backend/user_image_2.jpg);">

        <div class="profile profile-transparent">
            <div class="profile-image">
                <img src="img/backend/user_1.jpg">
                <div class="profile-badges">
                    <a href="#" class="profile-badges-left"><i class="fa fa-star"></i>Followes 243</a>
                    <a href="#" class="profile-badges-right"><i class="fa fa-money"></i>Rank/Credits 1,971</a>
                </div>
                <div class="profile-status online"></div>
            </div>
            <div class="profile-info">
                <h4>Devin Stephens</h4>
                <span><i class="fa fa-map-marker"></i> England, London</span>
            </div>
        </div>

        <div class="page-profile-stats">
            <div class="page-profile-stats-left">
                <div class="pull-right">
                    <img src="img/backend/user_2.jpg">
                    <img src="img/backend/user_3.jpg">
                    <img src="img/backend/user_4.jpg">
                    <img src="img/backend/user_5.jpg">
                    <div class="page-profile-stats-count">
                        My Favorites
                        <span>14,522</span>
                    </div>
                </div>
            </div>
            <div class="page-profile-stats-right">
                <div class="pull-left">
                    <div class="page-profile-stats-count">
                        Top 5 Models
                        <span>* * * * *</span>
                    </div>
                    <img src="img/backend/user_6.jpg">
                    <img src="img/backend/user_7.jpg">
                    <img src="img/backend/user_8.jpg">
                    <img src="img/backend/user_1.jpg">
                    <img src="img/backend/user_1.jpg">
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <div class="row">
            <div class="col-md-3">
                <div class="list-group">
                    <a href="{{ url('profile-show') }}" class="list-group-item"><i class="fa fa-star"></i> News</a>
                    <a href="{{ url('profile-edit') }}" class="list-group-item"><i class="fa fa-cog"></i> Settings</a>
                    <a href="{{ url('inbox') }}" class="list-group-item"><i class="fa fa-envelope"></i> My messages
                        <span
                                class="badge badge-danger">1</span></a>
                    <a href="{{ url('contacts') }}" class="list-group-item"><i class="fa fa-users"></i> Friends</a>
                    <a href="{{ url('#') }}" class="list-group-item"><i class="fa fa-photo"></i> Gallery</a>
                    <a href="{{ url('#') }}" class="list-group-item"><i class="fa fa-video-camera"></i> Videos</a>
                    <a href="{{ url('credits-add') }}" class="list-group-item"><i class="fa fa-money"></i> Add
                        credits</a>
                    <a href="{{ url('#') }}" class="list-group-item active"><i class="fa fa fa-shopping-cart"></i> Buy
                        gifts</a>
                </div>

            </div>
            <div class="col-md-9">
                <div class="col-md-12">
                    <div class="page-title" style="text-align:center;">
                        <h1>Add more gifts</h1>
                        <p>Here you have our collection of gifts</p>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/boot.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/leprechaun.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/leprechaun.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/horseshoe.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/tophat.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/pipe.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/boot.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/leprechaun.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/horseshoe.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/tophat.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/pipe.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/boot.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/leprechaun.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/horseshoe.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/tophat.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/pipe.png">
                            </div>
                            <div class="profile-info">
                                <h4>Devin Stephens</h4>
                            </div>
                            <div class="profile-buttons">
                                <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="margin-top: 30px" class="col-md-12">
                    <div class="page-title" style="text-align:center;">
                        <h1>Gift collection</h1>
                        <p>Here you have the gifts you bought so far</p>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/boot.png">
                                <p>12</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/leprechaun.png">
                                <p>4</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/horseshoe.png">
                                <p>20</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/tophat.png">
                                <p>7</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection