@extends('layouts.default')
@section('content')
    <div class="wrapper">
        <div class="row row-wider">
            <div class="col-md-3">
                <div class="profile margin-bottom-0">
                    <div class="profile-image">
                        <img src="img/backend/user_1.jpg">
                        <div class="profile-badges">
                            <a href="#" class="profile-badges-left"><i class="fa fa-plus"></i></a>
                            <a href="#" class="profile-badges-right"><i class="fa fa-camera"></i></a>
                        </div>
                        <div class="profile-status online"></div>
                    </div>
                    <div class="profile-info">
                        <h4>Devin Stephens</h4>
                        <span><i class="fa fa-map-marker"></i> UK, London</span>
                    </div>
                    <div class="profile-info text-left">
                        Profile complete on <strong>70%</strong>
                        <div class="progress progress-bar-xs">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
                                 aria-valuemin="0" aria-valuemax="100" style="width: 70%;"></div>
                        </div>
                    </div>
                </div>

                <div class="list-group">
                    <a href="{{ url('profile-edit') }}" class="list-group-item"><i class="fa fa-wrench"></i> Edit
                        profile</a>
                    <a href="{{ url('credits') }}" class="list-group-item"><i class="fa fa-cog"></i> My credits</a>
                    <a href="{{ url('gifts') }}" class="list-group-item active"><i class="fa fa-cog"></i> My gifts</a>
                    <a href="{{ url('inbox') }}" class="list-group-item"><i class="fa fa-comment"></i> Messages <span
                                class="badge badge-info">2</span></a>
                    <a href="{{ url('gallery') }}" class="list-group-item"><i class="fa fa-photo"></i> My Gallery</a>
                </div>
            </div>
            <div class="col-md-9">
                <div class="col-md-12">
                    <div class="page-title" style="text-align:center;">
                        <h1>Gift collection</h1>
                        <p>Here you have the gifts you bought so far</p>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/boot.png">
                                <p>12</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/boot.png">
                                <p>12</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/leprechaun.png">
                                <p>4</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/horseshoe.png">
                                <p>20</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/tophat.png">
                                <p>7</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/pipe.png">
                                <p>14</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/boot.png">
                                <p>12</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/leprechaun.png">
                                <p>4</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/horseshoe.png">
                                <p>20</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/tophat.png">
                                <p>7</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/pipe.png">
                                <p>14</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/boot.png">
                                <p>12</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/leprechaun.png">
                                <p>4</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/horseshoe.png">
                                <p>20</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/tophat.png">
                                <p>7</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="profile">
                            <div class="profile-image">
                                <img src="img/backend/pipe.png">
                                <p>14</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection