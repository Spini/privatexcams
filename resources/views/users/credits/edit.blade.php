@extends('layouts.default')
@section('content')
    <div class="page-profile" style="background-image: url(img/backend/user_image_2.jpg);">

        <div class="profile profile-transparent">
            <div class="profile-image">
                <img src="img/backend/user_1.jpg">
                <div class="profile-badges">
                    <a href="#" class="profile-badges-left"><i class="fa fa-star"></i>Followes 243</a>
                    <a href="#" class="profile-badges-right"><i class="fa fa-money"></i>Rank/Credits 1,971</a>
                </div>
                <div class="profile-status online"></div>
            </div>
            <div class="profile-info">
                <h4>Devin Stephens</h4>
                <span><i class="fa fa-map-marker"></i> England, London</span>
            </div>
        </div>

        <div class="page-profile-stats">
            <div class="page-profile-stats-left">
                <div class="pull-right">
                    <img src="img/backend/user_2.jpg">
                    <img src="img/backend/user_3.jpg">
                    <img src="img/backend/user_4.jpg">
                    <img src="img/backend/user_5.jpg">
                    <div class="page-profile-stats-count">
                        My Favorites
                        <span>14,522</span>
                    </div>
                </div>
            </div>
            <div class="page-profile-stats-right">
                <div class="pull-left">
                    <div class="page-profile-stats-count">
                        Top 5 Models
                        <span>* * * * *</span>
                    </div>
                    <img src="img/backend/user_6.jpg">
                    <img src="img/backend/user_7.jpg">
                    <img src="img/backend/user_8.jpg">
                    <img src="img/backend/user_1.jpg">
                    <img src="img/backend/user_1.jpg">
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <div class="row">
            <div class="col-md-3">
                <div class="list-group">
                    <a href="{{ url('profile-show') }}" class="list-group-item"><i class="fa fa-star"></i> News</a>
                    <a href="{{ url('profile-edit') }}" class="list-group-item"><i class="fa fa-cog"></i> Settings</a>
                    <a href="{{ url('inbox') }}" class="list-group-item"><i class="fa fa-envelope"></i> My messages
                        <span
                                class="badge badge-danger">1</span></a>
                    <a href="{{ url('contacts') }}" class="list-group-item"><i class="fa fa-users"></i> Friends</a>
                    <a href="{{ url('#') }}" class="list-group-item"><i class="fa fa-photo"></i> Gallery</a>
                    <a href="{{ url('#') }}" class="list-group-item"><i class="fa fa-video-camera"></i> Videos</a>
                    <a href="{{ url('credits-add') }}" class="list-group-item active"><i class="fa fa-money"></i> Add
                        credits</a>
                    <a href="{{ url('gifts-add') }}" class="list-group-item"><i class="fa fa fa-shopping-cart"></i> Buy
                        gifts</a>
                </div>

            </div>
            <div class="col-md-9">
                <div class="row" style="margin-bottom: 50px">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Type the value <span>(10,20,50,100,500,1000)</span></label>
                            <input type="text" class="form-control" placeholder="Value">
                        </div>
                        <button class="btn btn-danger" type="submit">Submit</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="dev-widget dev-widget">
                            <h2>Bronze</h2>
                            <p>+50 Free credits</p>
                            <div class="dev-stat"><span class="counter"> 200</span> credits</div>
                            <div class="progress progress-bar-xs">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 80%;"></div>
                            </div>
                            <p>Take the pack just of 19.99$ </p>

                            <a href="#" class="dev-drop">Buy now <span
                                        class="fa fa-angle-right pull-right"></span></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="dev-widget dev-widget-success">
                            <h2>Silver</h2>
                            <p>+150 Free credits</p>
                            <div class="dev-stat"><span class="counter"> 350</span> credits</div>
                            <div class="progress progress-bar-xs">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 80%;"></div>
                            </div>
                            <p>Take the pack just of 39.99$ </p>

                            <a href="#" class="dev-drop">Buy now <span
                                        class="fa fa-angle-right pull-right"></span></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="dev-widget dev-widget-success">
                            <h2>Gold</h2>
                            <p>+200 Free credits</p>
                            <div class="dev-stat"><span class="counter">700</span> credits</div>
                            <div class="progress progress-bar-xs">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 80%;"></div>
                            </div>
                            <p>Take the pack just of 79.99$ </p>

                            <a href="#" class="dev-drop">Buy now <span
                                        class="fa fa-angle-right pull-right"></span></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="dev-widget dev-widget-success">
                            <h2>Dimond</h2>
                            <p>+300 Free credits</p>
                            <div class="dev-stat"><span class="counter">1300</span> credits</div>
                            <div class="progress progress-bar-xs">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 80%;"></div>
                            </div>
                            <p>Take the pack just of 139.99$ </p>

                            <a href="#" class="dev-drop">Buy now <span
                                        class="fa fa-angle-right pull-right"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection