@extends('layouts.default')
@section('content')
    <div class="wrapper">
        <div class="row row-wider">
            <div class="col-md-3">
                <div class="profile margin-bottom-0">
                    <div class="profile-image">
                        <img src="img/backend/user_1.jpg">
                        <div class="profile-badges">
                            <a href="#" class="profile-badges-left"><i class="fa fa-plus"></i></a>
                            <a href="#" class="profile-badges-right"><i class="fa fa-camera"></i></a>
                        </div>
                        <div class="profile-status online"></div>
                    </div>
                    <div class="profile-info">
                        <h4>Devin Stephens</h4>
                        <span><i class="fa fa-map-marker"></i> UK, London</span>
                    </div>
                    <div class="profile-info text-left">
                        Profile complete on <strong>70%</strong>
                        <div class="progress progress-bar-xs">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
                                 aria-valuemin="0" aria-valuemax="100" style="width: 70%;"></div>
                        </div>
                    </div>
                </div>

                <div class="list-group">
                    <a href="{{ url('profile-edit') }}" class="list-group-item"><i class="fa fa-wrench"></i> Edit
                        profile</a>
                    <a href="{{ url('credits') }}" class="list-group-item active"><i class="fa fa-cog"></i> My
                        credits</a>
                    <a href="{{ url('gifts') }}" class="list-group-item"><i class="fa fa-cog"></i> My gifts</a>
                    <a href="{{ url('inbox') }}" class="list-group-item"><i class="fa fa-comment"></i> Messages <span
                                class="badge badge-info">2</span></a>
                    <a href="{{ url('#') }}" class="list-group-item"><i class="fa fa-photo"></i> My Gallery</a>
                </div>
            </div>
            <div class="col-md-9">
                <div class="dev-col">

                    <div class="dev-widget dev-widget-transparent dev-widget-danger">
                        <h2>Your Balance</h2>
                        <p>All your credits for this time</p>
                        <div class="dev-stat"><span class="counter">5,321</span> credits</div>
                        <div class="progress progress-bar-xs">
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60"
                                 aria-valuemin="0" aria-valuemax="100" style="width: 80%;"></div>
                        </div>
                        <p>You can add more credits to buy gifts or premium rank. Also you can spend it on our
                            marketplace.</p>

                        <a href="{{ url('credits-add') }}" class="dev-drop">Add more<span
                                    class="fa fa-angle-right pull-right"></span></a>
                    </div>

                </div>
                <div style="margin-top: 5%" class="col-md-6">
                    <div class="page-subtitle">
                        <h3>My acquisitions</h3>
                        <p>Use this feature to show actions for some period of time</p>
                    </div>

                    <ul class="timeline-simple">
                        <li>
                            <span class="timeline-simple-date">12 min ago</span>
                            The standard Lorem Ipsum passage, used since the 1500s
                        </li>
                        <li>
                            <span class="timeline-simple-date">2h ago</span>
                            Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
                        </li>
                        <li>
                            <span class="timeline-simple-date">15h ago</span>
                            1914 translation by H. Rackham
                        </li>
                        <li>
                            <span class="timeline-simple-date">19h ago</span>
                            Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
                        </li>
                    </ul>

                </div>
                <div style="margin-top: 5%" class="col-md-6">
                    <div class="page-subtitle">
                        <h3>My spendings</h3>
                        <p>Use this feature to show actions for some period of time</p>
                    </div>

                    <ul class="timeline-simple">
                        <li>
                            <span class="timeline-simple-date">12 min ago</span>
                            The standard Lorem Ipsum passage, used since the 1500s
                        </li>
                        <li>
                            <span class="timeline-simple-date">2h ago</span>
                            Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
                        </li>
                        <li>
                            <span class="timeline-simple-date">15h ago</span>
                            1914 translation by H. Rackham
                        </li>
                        <li>
                            <span class="timeline-simple-date">19h ago</span>
                            Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
@endsection