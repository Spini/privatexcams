@extends('layouts.horizontal')
@section('content')
    <div class="wrapper">
        <div class="col-md-6 col-md-offset-3">
            <div class="page-subtitle page-subtitle-centralized">
                <h3>Search for your friends</h3>
                <p>Use this search to find your friends faster</p>
            </div>

            <form id="dev-faq-form">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" id="dev-faq-keyword" placeholder="Search...">
                        <div class="input-group-btn">
                            <button class="btn btn-default" id="dev-faq-search"><i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <p id="dev-faq-search-result"></p>
            </form>

        </div>
        <div class="wrapper">

            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">

                    <div class="profile">
                        <div class="profile-image">
                            <img src="img/backend/user_1.jpg">
                            <div class="profile-badges">
                                <a href="#" class="profile-badges-left"><i class="fa fa-trophy"></i> 243</a>
                                <a href="#" class="profile-badges-right"><i class="fa fa-users"></i> 1,971</a>
                            </div>
                            <div class="profile-status online"></div>
                        </div>
                        <div class="profile-info">
                            <h4>Devin Stephens</h4>
                            <span><i class="fa fa-map-marker"></i> England, London</span>
                        </div>
                        <div class="profile-buttons">
                            <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-info"></i>
                            </button>
                            <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-phone"></i>
                            </button>
                        </div>
                    </div>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">

                    <div class="profile">
                        <div class="profile-image">
                            <img src="img/backend/user_2.jpg">
                            <div class="profile-badges">
                                <a href="#" class="profile-badges-left"><i class="fa fa-trophy"></i> 75</a>
                                <a href="#" class="profile-badges-right"><i class="fa fa-users"></i> 866</a>
                            </div>
                            <div class="profile-status online"></div>
                        </div>
                        <div class="profile-info">
                            <h4>Shannon Freeman</h4>
                            <span><i class="fa fa-map-marker"></i> USA, New York</span>
                        </div>
                        <div class="profile-buttons">
                            <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-info"></i>
                            </button>
                            <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-phone"></i>
                            </button>
                        </div>
                    </div>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">

                    <div class="profile">
                        <div class="profile-image">
                            <img src="img/backend/user_3.jpg">
                            <div class="profile-badges">
                                <a href="#" class="profile-badges-left"><i class="fa fa-trophy"></i> 56</a>
                                <a href="#" class="profile-badges-right"><i class="fa fa-users"></i> 738</a>
                            </div>
                            <div class="profile-status online"></div>
                        </div>
                        <div class="profile-info">
                            <h4>Marissa George</h4>
                            <span><i class="fa fa-map-marker"></i> Ukraine, Kiev</span>
                        </div>
                        <div class="profile-buttons">
                            <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-info"></i>
                            </button>
                            <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-phone"></i>
                            </button>
                        </div>
                    </div>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">

                    <div class="profile">
                        <div class="profile-image">
                            <img src="img/backend/user_3.jpg">
                            <div class="profile-badges">
                                <a href="#" class="profile-badges-left"><i class="fa fa-trophy"></i> 56</a>
                                <a href="#" class="profile-badges-right"><i class="fa fa-users"></i> 738</a>
                            </div>
                            <div class="profile-status online"></div>
                        </div>
                        <div class="profile-info">
                            <h4>Marissa George</h4>
                            <span><i class="fa fa-map-marker"></i> Ukraine, Kiev</span>
                        </div>
                        <div class="profile-buttons">
                            <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-info"></i>
                            </button>
                            <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-phone"></i>
                            </button>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">

                    <div class="profile">
                        <div class="profile-image">
                            <img src="img/backend/user_5.jpg">
                            <div class="profile-badges">
                                <a href="#" class="profile-badges-left"><i class="fa fa-trophy"></i> 243</a>
                                <a href="#" class="profile-badges-right"><i class="fa fa-users"></i> 1,971</a>
                            </div>
                            <div class="profile-status away"></div>
                        </div>
                        <div class="profile-info">
                            <h4>Karen Spencer</h4>
                            <span><i class="fa fa-map-marker"></i> Russia, Moscow</span>
                        </div>
                        <div class="profile-buttons">
                            <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-info"></i>
                            </button>
                            <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-phone"></i>
                            </button>
                        </div>
                    </div>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">

                    <div class="profile">
                        <div class="profile-image">
                            <img src="img/backend/user_6.jpg">
                            <div class="profile-badges">
                                <a href="#" class="profile-badges-left"><i class="fa fa-trophy"></i> 75</a>
                                <a href="#" class="profile-badges-right"><i class="fa fa-users"></i> 866</a>
                            </div>
                            <div class="profile-status away"></div>
                        </div>
                        <div class="profile-info">
                            <h4>Darrell Wolfe</h4>
                            <span><i class="fa fa-map-marker"></i> USA, Hollywood</span>
                        </div>
                        <div class="profile-buttons">
                            <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-info"></i>
                            </button>
                            <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-phone"></i>
                            </button>
                        </div>
                    </div>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">

                    <div class="profile">
                        <div class="profile-image">
                            <img src="img/backend/user_7.jpg">
                            <div class="profile-badges">
                                <a href="#" class="profile-badges-left"><i class="fa fa-trophy"></i> 56</a>
                                <a href="#" class="profile-badges-right"><i class="fa fa-users"></i> 738</a>
                            </div>
                            <div class="profile-status offline"></div>
                        </div>
                        <div class="profile-info">
                            <h4>Sydney Reeves</h4>
                            <span><i class="fa fa-map-marker"></i> Ukraine, Lvov</span>
                        </div>
                        <div class="profile-buttons">
                            <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-info"></i>
                            </button>
                            <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-phone"></i>
                            </button>
                        </div>
                    </div>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">

                    <div class="profile">
                        <div class="profile-image">
                            <img src="img/backend/user_7.jpg">
                            <div class="profile-badges">
                                <a href="#" class="profile-badges-left"><i class="fa fa-trophy"></i> 56</a>
                                <a href="#" class="profile-badges-right"><i class="fa fa-users"></i> 738</a>
                            </div>
                            <div class="profile-status offline"></div>
                        </div>
                        <div class="profile-info">
                            <h4>Sydney Reeves</h4>
                            <span><i class="fa fa-map-marker"></i> Ukraine, Lvov</span>
                        </div>
                        <div class="profile-buttons">
                            <button class="btn btn-primary btn-clean btn-rounded"><i class="fa fa-info"></i>
                            </button>
                            <button class="btn btn-danger btn-clean btn-rounded"><i class="fa fa-phone"></i>
                            </button>
                        </div>
                    </div>

                </div>
            </div>

            <nav class="pull-right">
                <ul class="pagination">
                    <li class="active">
                        <a href="#">1</a>
                    </li>
                    <li>
                        <a href="#">2</a>
                    </li>
                    <li>
                        <a href="#">3</a>
                    </li>
                    <li>
                        <a href="#">4</a>
                    </li>
                    <li>
                        <a href="#">5</a>
                    </li>
                </ul>
            </nav>

        </div>
    </div>
@endsection