@extends('layouts.horizontal')
@section('content')

    <div class="wrapper">
        <div class="row row-wider">
            <div class="col-md-3">
                <div class="profile margin-bottom-0">
                    <div class="profile-image">
                        <img src="img/backend/user_1.jpg">
                        <div class="profile-badges">
                            <a href="#" class="profile-badges-left"><i class="fa fa-plus"></i></a>
                            <a href="#" class="profile-badges-right"><i class="fa fa-camera"></i></a>
                        </div>
                        <div class="profile-status online"></div>
                    </div>
                    <div class="profile-info">
                        <h4>Devin Stephens</h4>
                        <span><i class="fa fa-map-marker"></i> UK, London</span>
                    </div>
                    <div class="profile-info text-left">
                        Profile complete on <strong>70%</strong>
                        <div class="progress progress-bar-xs">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
                                 aria-valuemin="0" aria-valuemax="100" style="width: 70%;"></div>
                        </div>
                    </div>
                </div>

                <div class="list-group">
                    <a href="{{ url('studio-edit') }}" class="list-group-item active"><i class="fa fa-wrench"></i> Edit
                        profile</a>
                    <a href="{{ url('#') }}" class="list-group-item"><i class="fa fa-cog"></i> My profit</a>
                    <a href="{{ url('inbox') }}" class="list-group-item"><i class="fa fa-comment"></i> Messages <span
                                class="badge badge-info">2</span></a>
                    <a href="{{ url('gallery') }}" class="list-group-item"><i class="fa fa-photo"></i> My Gallery</a>
                </div>

                <div class="panel panel-default panel-muted">

                    <div class="panel-body" style="border-bottom: 0px;">
                        <div class="page-subtitle page-subtitle-centralized margin-bottom-0">
                            <h3>My Friends(Users)</h3>
                            <p>You can remove anyone from your contacts</p>
                        </div>
                        <button class="btn btn-primary btn-rounded btn-clean pull-right">Add friends</button>
                    </div>

                    <div class="panel-body list-contacts" style="border-bottom: 0px;">
                        <a href="#" class="list-contacts-item contact-online">
                            <img src="img/backend/user_1.jpg"> Devin Stephens
                            <button class="btn btn-danger btn-clean btn-rounded btn-only-icon pull-right"><i
                                        class="fa fa-times"></i></button>
                        </a>
                        <a href="#" class="list-contacts-item contact-online">
                            <img src="img/backend/user_2.jpg"> John Doe
                            <button class="btn btn-danger btn-clean btn-rounded btn-only-icon pull-right"><i
                                        class="fa fa-times"></i></button>
                        </a>
                        <a href="#" class="list-contacts-item contact-away">
                            <img src="img/backend/user_3.jpg"> Marissa George
                            <button class="btn btn-danger btn-clean btn-rounded btn-only-icon pull-right"><i
                                        class="fa fa-times"></i></button>
                        </a>
                        <a href="#" class="list-contacts-item contact-away">
                            <img src="img/backend/user_4.jpg"> Sydney Reeves
                            <button class="btn btn-danger btn-clean btn-rounded btn-only-icon pull-right"><i
                                        class="fa fa-times"></i></button>
                        </a>
                        <a href="#" class="list-contacts-item contact-offline">
                            <img src="img/backend/user_5.jpg"> Karen Spencer
                            <button class="btn btn-danger btn-clean btn-rounded btn-only-icon pull-right"><i
                                        class="fa fa-times"></i></button>
                        </a>
                    </div>
                    <div class="panel-body" style="border-top: 0px;">
                        <button class="btn btn-primary btn-rounded btn-clean pull-right">Load more</button>
                    </div>
                </div>
            </div>
            <div class="col-md-9">

                <div class="page-subtitle margin-bottom-0">
                    <h3>Authentication details and personal information</h3>
                    <p>Important fields for your identity in our system</p>
                </div>
                <div class="row">
                    <form id="validate" role="form" action="javascript:alert('Form #validate submited');">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Login</label>
                                <input type="text" class="form-control" name="login"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" id="password2"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" class="form-control" name="re-password"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" value=" "/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Second Name</label>
                                <input type="text" class="form-control" value=" "/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="text" value="" name="email" class="form-control"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Country</label>
                                <input type="text" class="form-control" value=" "/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control" value=" "/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Date of birth:</label>
                                <input type="text" class="form-control datepicker" name="date"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Company name</label>
                                <input type="text" class="form-control" value=" "/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Registration number</label>
                                <input type="text" class="form-control" value=" "/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>V.A.T. Number</label>
                                <input type="text" class="form-control" value=" "/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" value=" "/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="wrapper">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="page-subtitle">
                                <h3>Load your documents</h3>
                                <p>Id photo</p>
                            </div>
                            <form action="#" class="dropzone dropzone-mini"></form>
                        </div>
                        <div style="margin-top: 32px" class="col-md-6">
                            <div class="page-subtitle">
                                <p>Id next to the face</p>
                            </div>
                            <form action="#" class="dropzone dropzone-mini"></form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" style="margin-top: 30px">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_default">View
                        contract
                    </button>
                </div>
                <div class="col-md-6" style="margin-top: 30px">
                    <button class="btn btn-danger pull-right">Download and sign</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('plugins')
<script src="{{ asset('js/backend/dropzone.min.js') }}"></script>
@endpush

