@extends('layouts.default')
@section('content')
    <div class="wrapper">
        <div class="row row-wider">
            <div class="col-md-3">
                <div class="profile margin-bottom-0">
                    <div class="profile-image">
                        <img src="img/backend/user_1.jpg">
                        <div class="profile-badges">
                            <a href="#" class="profile-badges-left"><i class="fa fa-plus"></i></a>
                            <a href="#" class="profile-badges-right"><i class="fa fa-camera"></i></a>
                        </div>
                        <div class="profile-status online"></div>
                    </div>
                    <div class="profile-info">
                        <h4>Devin Stephens</h4>
                        <span><i class="fa fa-map-marker"></i> UK, London</span>
                    </div>
                    <div class="profile-info text-left">
                        Profile complete on <strong>70%</strong>
                        <div class="progress progress-bar-xs">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60"
                                 aria-valuemin="0" aria-valuemax="100" style="width: 70%;"></div>
                        </div>
                    </div>
                </div>
                @php($settings_menu = App\Menu::userSettings())
                 <div class="list-group">
                    @foreach($settings_menu as $item)
                         <a href="{{ $item['url'] }}" class="list-group-item {{ App\Menu::isUrl( $item['url'] )}}">
                             <i class="{{ $item['icon'] }}"></i>
                            {!! $item['display'] !!}
                         </a>
                    @endforeach
                </div>

                <div class="panel panel-default panel-muted">

                    <div class="panel-body" style="border-bottom: 0px;">
                        <div class="page-subtitle page-subtitle-centralized margin-bottom-0">
                            <h3>My Friends(Users)</h3>
                            <p>You can remove anyone from your contacts</p>
                        </div>
                        <button class="btn btn-primary btn-rounded btn-clean pull-right">Add friends</button>
                    </div>

                    <div class="panel-body list-contacts" style="border-bottom: 0px;">
                        <a href="#" class="list-contacts-item contact-online">
                            <img src="img/backend/user_1.jpg"> Devin Stephens
                            <button class="btn btn-danger btn-clean btn-rounded btn-only-icon pull-right"><i
                                        class="fa fa-times"></i></button>
                        </a>
                        <a href="#" class="list-contacts-item contact-online">
                            <img src="img/backend/user_2.jpg"> John Doe
                            <button class="btn btn-danger btn-clean btn-rounded btn-only-icon pull-right"><i
                                        class="fa fa-times"></i></button>
                        </a>
                        <a href="#" class="list-contacts-item contact-away">
                            <img src="img/backend/user_3.jpg"> Marissa George
                            <button class="btn btn-danger btn-clean btn-rounded btn-only-icon pull-right"><i
                                        class="fa fa-times"></i></button>
                        </a>
                        <a href="#" class="list-contacts-item contact-away">
                            <img src="img/backend/user_4.jpg"> Sydney Reeves
                            <button class="btn btn-danger btn-clean btn-rounded btn-only-icon pull-right"><i
                                        class="fa fa-times"></i></button>
                        </a>
                        <a href="#" class="list-contacts-item contact-offline">
                            <img src="img/backend/user_5.jpg"> Karen Spencer
                            <button class="btn btn-danger btn-clean btn-rounded btn-only-icon pull-right"><i
                                        class="fa fa-times"></i></button>
                        </a>
                    </div>
                    <div class="panel-body" style="border-top: 0px;">
                        <button class="btn btn-primary btn-rounded btn-clean pull-right">Load more</button>
                    </div>
                </div>
            </div>
            @yield('edit')
        </div>
    </div>
@endsection