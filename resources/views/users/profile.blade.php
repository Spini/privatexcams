@extends('layouts.default')
@section('content')
    @yield('head')
    <div class="wrapper">
        <div class="row">
            <div class="col-md-3">
                @php($profile_menu = App\Menu::userProfile())

                <div class="list-group">
                    @foreach($profile_menu as $item)
                         <a href="{{ $item['url'] }}" class="list-group-item {{ App\Menu::isUrl( $item['url'] )}}">
                             <i class="{{ $item['icon'] }}"></i>
                            {!! $item['display'] !!}
                         </a>
                    @endforeach
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="page-subtitle">
                            <h3>Quick Message to Admin</h3>
                            <p>Send e-mail message to this user</p>
                        </div>
                        <div class="form-group">
                            <label>Subject</label>
                            <input type="email" class="form-control" placeholder="Message subject">
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea class="form-control" placeholder="Your message" rows="3"></textarea>
                        </div>
                        <button class="btn btn-danger pull-right">Send</button>
                    </div>
                </div>

            </div>
            @yield('show')
        </div>
    </div>
@endsection
