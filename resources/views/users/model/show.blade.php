@extends('users.profile')
@section('head')
    <div class="page-profile" style="background-image: url(img/backend/user_image_2.jpg);">

        <div class="profile profile-transparent">
            <div class="profile-image">
                <img src="img/backend/user_1.jpg">
                <div class="profile-badges">
                    <a href="#" class="profile-badges-left"><i class="fa fa-twitter"></i>Follows 243</a>
                    <a href="#" class="profile-badges-right"><i class="fa fa-bitcoin"></i>Profit 1,971</a>
                </div>
                <div class="profile-status online"></div>
            </div>
            <div class="profile-info">
                <h4>Devin Stephens</h4>
                <span><i class="fa fa-map-marker"></i> England, London</span>
            </div>
        </div>

        <div class="page-profile-stats">
            <div class="page-profile-stats-left">
                <div class="pull-right">
                    <img src="img/backend/user_2.jpg">
                    <img src="img/backend/user_3.jpg">
                    <img src="img/backend/user_4.jpg">
                    <img src="img/backend/user_5.jpg">
                    <img src="img/backend/user_5.jpg">
                    <div class="page-profile-stats-count">
                        Followers
                        <span>14,522</span>
                    </div>
                </div>
            </div>
            <div class="page-profile-stats-right">
                <div class="pull-left">
                    <div class="page-profile-stats-count">
                        Your rating
                        <span>* * * * *</span>
                    </div>
                    <img src="img/backend/user_6.jpg">
                    <img src="img/backend/user_7.jpg">
                    <img src="img/backend/user_8.jpg">
                    <img src="img/backend/user_1.jpg">
                    <img src="img/backend/user_1.jpg">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('show')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="page-subtitle">
                    <h3>Rules</h3>
                    <p>Here you have the rules. Check them out.</p>
                </div>

                <div class="panel panel-default panel-collapsed">
                    <div class="panel-heading">
                        <h3 class="panel-title">Read the rules here</h3>
                        <ul class="panel-btn">
                            <li><a href="#" class="btn btn-danger panel-collapse"><i class="fa fa-angle-up"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <p>Aliquam et ex accumsan, maximus nibh ac, sagittis magna. Nam et ante facilisis,
                            venenatis ligula eleifend, convallis est.After reading the January 1975 issue of
                            Popular Electronics that demonstrated the Altair 8800, Gates contacted Micro
                            Instrumentation and Telemetry Systems (MITS), the creators of the new microcomputer,
                            to inform them that he and others were working on a BASIC interpreter for the
                            platform.</p>
                    </div>
                </div>

            </div>
            <div class="col-md-12">
                <div class="page-subtitle">
                    <h3>Newsfeed</h3>
                    <p>Check it out the news</p>
                </div>
                <ul class="timeline-simple">
                    <li>
                        <span class="timeline-simple-date">12 min ago</span>
                        The standard Lorem Ipsum passage, used since the 1500s
                    </li>
                    <li>
                        <span class="timeline-simple-date">2h ago</span>
                        Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
                    </li>
                    <li>
                        <span class="timeline-simple-date">15h ago</span>
                        1914 translation by H. Rackham
                    </li>
                    <li>
                        <span class="timeline-simple-date">19h ago</span>
                        Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
                    </li>
                </ul>
            </div>
            <div class="col-md-12">
                <div class="page-title" style="text-align:center;">
                    <h1>My gifts</h1>
                    <p>Here you have the gifts you got so far</p>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="profile">
                        <div class="profile-image">
                            <img src="img/backend/boot.png">
                            <p>12</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="profile">
                        <div class="profile-image">
                            <img src="img/backend/leprechaun.png">
                            <p>4</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="profile">
                        <div class="profile-image">
                            <img src="img/backend/horseshoe.png">
                            <p>20</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="profile">
                        <div class="profile-image">
                            <img src="img/backend/tophat.png">
                            <p>7</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection