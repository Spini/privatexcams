@extends('users.profile')
@section('head')
    <div class="page-profile" style="background-image: url(img/backend/user_image_2.jpg);">

        <div class="profile profile-transparent">
            <div class="profile-image">
                <img src="img/backend/user_1.jpg">
                <div class="profile-badges">
                    <a href="#" class="profile-badges-left"><i class="fa fa-star"></i>Followes 243</a>
                    <a href="#" class="profile-badges-right"><i class="fa fa-money"></i>Rank/Credits 1,971</a>
                </div>
                <div class="profile-status online"></div>
            </div>
            <div class="profile-info">
                <h4>Devin Stephens</h4>
                <span><i class="fa fa-map-marker"></i> England, London</span>
            </div>
        </div>

        <div class="page-profile-stats">
            <div class="page-profile-stats-left">
                <div class="pull-right">
                    <img src="img/backend/user_2.jpg">
                    <img src="img/backend/user_3.jpg">
                    <img src="img/backend/user_4.jpg">
                    <img src="img/backend/user_5.jpg">
                    <img src="img/backend/user_5.jpg">
                    <div class="page-profile-stats-count">
                        My Favorites
                        <span>14,522</span>
                    </div>
                </div>
            </div>
            <div class="page-profile-stats-right">
                <div class="pull-left">
                    <div class="page-profile-stats-count">
                        Top 5 Models
                        <span>* * * * *</span>
                    </div>
                    <img src="img/backend/user_6.jpg">
                    <img src="img/backend/user_7.jpg">
                    <img src="img/backend/user_8.jpg">
                    <img src="img/backend/user_1.jpg">
                    <img src="img/backend/user_1.jpg">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('show')
    <div class="col-md-9">
        <div class="page-title" style="text-align:center;">
            <h1>New models</h1>
            <p>Newest four models on the website</p>
        </div>
        <div class="col-md-6">
            <div class="widget-news widget-news-primary">
                <div class="widget-news-content">

                    <div class="profile profile-transparent margin-bottom-10 margin-top-10">
                        <div class="profile-image">
                            <img src="img/backend/user_6.jpg">
                            <div class="profile-badges">
                                <a href="#" class="profile-badges-left"><i class="fa fa-trophy"></i>
                                    Followes</a>
                                <a href="#" class="profile-badges-right"><i class="fa fa-users"></i>
                                    Price/min</a>
                            </div>
                            <div class="profile-status away"></div>
                        </div>
                        <div class="profile-info">
                            <h4>Darrell Wolfe</h4>
                            <span><i class="fa fa-map-marker"></i> USA, Hollywood</span>
                        </div>
                        <div class="profile-buttons">
                            <button class="btn btn-twitter btn-rounded padding-left-20 padding-right-20"><i
                                        class="fa fa-twitter"></i> Follow
                            </button>
                        </div>
                    </div>

                </div>
                <div class="widget-news-footer two-column">
                    <div>Followers: <strong>1,522</strong></div>
                    <div>Following: <strong>2,912</strong></div>
                </div>
            </div>
            <div class="widget-news widget-news-primary">
                <div class="widget-news-content">

                    <div class="profile profile-transparent margin-bottom-10 margin-top-10">
                        <div class="profile-image">
                            <img src="img/backend/user_6.jpg">
                            <div class="profile-badges">
                                <a href="#" class="profile-badges-left"><i class="fa fa-trophy"></i> 75</a>
                                <a href="#" class="profile-badges-right"><i class="fa fa-users"></i> 866</a>
                            </div>
                            <div class="profile-status away"></div>
                        </div>
                        <div class="profile-info">
                            <h4>Darrell Wolfe</h4>
                            <span><i class="fa fa-map-marker"></i> USA, Hollywood</span>
                        </div>
                        <div class="profile-buttons">
                            <button class="btn btn-twitter btn-rounded padding-left-20 padding-right-20"><i
                                        class="fa fa-twitter"></i> Follow
                            </button>
                        </div>
                    </div>

                </div>
                <div class="widget-news-footer two-column">
                    <div>Followers: <strong>1,522</strong></div>
                    <div>Following: <strong>2,912</strong></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="widget-news widget-news-primary">
                <div class="widget-news-content">

                    <div class="profile profile-transparent margin-bottom-10 margin-top-10">
                        <div class="profile-image">
                            <img src="img/backend/user_6.jpg">
                            <div class="profile-badges">
                                <a href="#" class="profile-badges-left"><i class="fa fa-trophy"></i> 75</a>
                                <a href="#" class="profile-badges-right"><i class="fa fa-users"></i> 866</a>
                            </div>
                            <div class="profile-status away"></div>
                        </div>
                        <div class="profile-info">
                            <h4>Darrell Wolfe</h4>
                            <span><i class="fa fa-map-marker"></i> USA, Hollywood</span>
                        </div>
                        <div class="profile-buttons">
                            <button class="btn btn-twitter btn-rounded padding-left-20 padding-right-20"><i
                                        class="fa fa-twitter"></i> Follow
                            </button>
                        </div>
                    </div>

                </div>
                <div class="widget-news-footer two-column">
                    <div>Followers: <strong>1,522</strong></div>
                    <div>Following: <strong>2,912</strong></div>
                </div>
            </div>
            <div class="widget-news widget-news-primary">
                <div class="widget-news-content">

                    <div class="profile profile-transparent margin-bottom-10 margin-top-10">
                        <div class="profile-image">
                            <img src="img/backend/user_6.jpg">
                            <div class="profile-badges">
                                <a href="#" class="profile-badges-left"><i class="fa fa-trophy"></i> 75</a>
                                <a href="#" class="profile-badges-right"><i class="fa fa-users"></i> 866</a>
                            </div>
                            <div class="profile-status away"></div>
                        </div>
                        <div class="profile-info">
                            <h4>Darrell Wolfe</h4>
                            <span><i class="fa fa-map-marker"></i> USA, Hollywood</span>
                        </div>
                        <div class="profile-buttons">
                            <button class="btn btn-twitter btn-rounded padding-left-20 padding-right-20"><i
                                        class="fa fa-twitter"></i> Follow
                            </button>
                        </div>
                    </div>

                </div>
                <div class="widget-news-footer two-column">
                    <div>Followers: <strong>1,522</strong></div>
                    <div>Following: <strong>2,912</strong></div>
                </div>
            </div>
        </div>
        <div class="row">

        </div>
        <div class="col-md-12">
            <div class="page-title" style="text-align:center;">
                <h1>Gift collection</h1>
                <p>Here you have the gifts you bought so far</p>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="profile">
                    <div class="profile-image">
                        <img src="img/backend/boot.png">
                        <p>12</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="profile">
                    <div class="profile-image">
                        <img src="img/backend/horseshoe.png">
                        <p>20</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="profile">
                    <div class="profile-image">
                        <img src="img/backend/tophat.png">
                        <p>7</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="profile">
                    <div class="profile-image">
                        <img src="img/backend/pipe.png">
                        <p>14</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection