@extends('users.profile')
@section('head')
    <div class="page-profile" style="background-image: url(img/backend/user_image_2.jpg);">

        <div class="profile profile-transparent">
            <div class="profile-image">
                <img src="img/backend/user_1.jpg">
                <div class="profile-badges">
                    <a href="#" class="profile-badges-left"><i class="fa fa-star"></i>Followes 243</a>
                    <a href="#" class="profile-badges-right"><i class="fa fa-money"></i>Rank/Credits 1,971</a>
                </div>
                <div class="profile-status online"></div>
            </div>
            <div class="profile-info">
                <h4>Devin Stephens</h4>
                <span><i class="fa fa-map-marker"></i> England, London</span>
            </div>
        </div>

        <div class="page-profile-stats">
            <div class="page-profile-stats-left">
                <div class="pull-right">
                    <img src="img/backend/user_2.jpg">
                    <img src="img/backend/user_3.jpg">
                    <img src="img/backend/user_4.jpg">
                    <img src="img/backend/user_5.jpg">
                    <img src="img/backend/user_5.jpg">
                    <div class="page-profile-stats-count">
                        My Favorites
                        <span>14,522</span>
                    </div>
                </div>
            </div>
            <div class="page-profile-stats-right">
                <div class="pull-left">
                    <div class="page-profile-stats-count">
                        Top 5 Models
                        <span>* * * * *</span>
                    </div>
                    <img src="img/backend/user_6.jpg">
                    <img src="img/backend/user_7.jpg">
                    <img src="img/backend/user_8.jpg">
                    <img src="img/backend/user_1.jpg">
                    <img src="img/backend/user_1.jpg">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('show')
    <div class="col-md-9">
        <div class="col-md-6">
            <video width="100%" controls>
                <source src="{{ asset('img/backend/Sinner.mp4') }}" type="video/mp4">
            </video>
        </div>
        <div class="col-md-6">
            <video width="100%" controls>
                <source src="{{ asset('img/backend/Sinner.mp4') }}" type="video/mp4">
            </video>
        </div>
        <div class="col-md-6">
            <video width="100%" controls>
                <source src="{{ asset('img/backend/Sinner.mp4') }}" type="video/mp4">
            </video>
        </div>
        <div class="col-md-6">
            <video width="100%" controls>
                <source src="{{ asset('img/backend/Sinner.mp4') }}" type="video/mp4">
            </video>
        </div>
    </div>
@endsection