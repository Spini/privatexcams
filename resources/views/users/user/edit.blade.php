@extends('users.settings')
@section('edit')
    <div class="col-md-9">

        <div class="page-subtitle margin-bottom-0">
            <h3>Authentication details and personal information</h3>
            <p>Important fields for your identity in our system</p>
        </div>
        <div class="row">
            <form id="validate" role="form" action="javascript:alert('Form #validate submited');">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Login:</label>
                        <input type="text" class="form-control" name="login"/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Password:</label>
                        <input type="password" class="form-control" name="password" id="password2"/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Confirm Password:</label>
                        <input type="password" class="form-control" name="re-password"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control" value=" "/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Second Name</label>
                        <input type="text" class="form-control" value=" "/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>E-mail: <span>required email</span></label>
                        <input type="text" value="" name="email" class="form-control"/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Country</label>
                        <input type="text" class="form-control" value=" "/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>City</label>
                        <input type="text" class="form-control" value=" "/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Date of birth:</label>
                        <input type="text" class="form-control datepicker" name="date"/>
                    </div>
                </div>
            </form>
        </div>

        <div class="page-subtitle margin-bottom-0">
            <h3>Public information</h3>
            <p>This information is important for us. If you want to make it public check the boxes.</p>
            <div class="row">
                <form id="validate" role="form" action="javascript:alert('Form #validate submited');">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Nickname</label>
                            <input type="text" class="form-control" name="login"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Age</label>
                            <input type="text" class="form-control" name="age"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Ethnicity</label>
                            <select class="form-control selectpicker">
                                <option value="1">Default</option>
                                <option value="2">Black</option>
                                <option value="3">Asiatic</option>
                                <option value="4">Hispanic</option>
                                <option value="5">Caucazian</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Languages</label>
                            <select class="form-control selectpicker" multiple>
                                <option value="1">English</option>
                                <option value="2">Italian</option>
                                <option value="3">German</option>
                                <option value="4">Spanish</option>
                                <option value="5">Russian</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Gender</label>
                            <select class="form-control selectpicker">
                                <option value="1">Default</option>
                                <option value="2">Male</option>
                                <option value="3">Female</option>
                                <option value="4">Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Sexuality</label>
                            <select class="form-control selectpicker">
                                <option value="1">Default</option>
                                <option value="2">Straight</option>
                                <option value="2">Gay</option>
                                <option value="2">Lesbi</option>
                                <option value="3">Bi</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Breast size</label>
                            <select class="form-control selectpicker">
                                <option value="1">Default</option>
                                <option value="2">Small</option>
                                <option value="3">Average</option>
                                <option value="4">Big</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Willingness</label>
                            <select class="form-control selectpicker" multiple>
                                <option value="1">Anal Sex</option>
                                <option value="2">Striptease</option>
                                <option value="3">Smoking</option>
                                <option value="4">Squirt</option>
                                <option value="5">Fingering</option>
                                <option value="6">Close up</option>
                                <option value="7">Footjob</option>
                                <option value="8">Submissive</option>
                                <option value="9">Dominant</option>
                                <option value="10">Live orgasm</option>
                                <option value="11">Sex toys</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Hair color</label>
                            <select class="form-control selectpicker">
                                <option value="1">Default</option>
                                <option value="2">Blonde</option>
                                <option value="3">Brown</option>
                                <option value="4">Black</option>
                                <option value="5">Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Eyes color</label>
                            <select class="form-control selectpicker">
                                <option value="1">Default</option>
                                <option value="2">Brown</option>
                                <option value="3">Blue</option>
                                <option value="4">Green</option>
                                <option value="5">Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Features</label>
                            <select class="form-control selectpicker" multiple>
                                <option value="1">Shaved</option>
                                <option value="2">Hairy pussy</option>
                                <option value="3">Piercing</option>
                                <option value="4">Tattoo</option>
                                <option value="5">Stockings</option>
                                <option value="6">Pregnant</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" placeholder="Your descriptiion"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-danger pull-right">Save</button>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('plugins')
<script src="{{ asset('js/backend/spectrum.js') }}"></script>
<script src="{{ asset('js/backend/bootstrap-datetimepicker.js') }}"></script>
@endpush