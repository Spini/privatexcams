<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta section -->
    <title>Intuitive - Admin Dashboard Template</title>
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="icon" href="img/backend/favicon.ico" type="image/x-icon">
    @stack('css')
    <link rel="stylesheet" type="text/css" href="css/backend/default-blue-white.css" id="dev-css">
    <link rel="stylesheet" type="text/css" href="css/backend/child.css" id="dev-css">

    {{--<link rel="stylesheet" type="text/css" href="css/dev-other/dev-ie-fix.css">--}}
    <script type="text/javascript" src="js/backend/modernizr.js"></script>
    <style>
        .dev-page {
            visibility: hidden;
        }
    </style>
    <script>
        window.Laravel = <?php echo json_encode( [ 'csrfToken' => csrf_token(), ] ); ?>
    </script>
</head>
<body>
<!-- set loading layer -->
<div class="dev-page-loading preloader"></div>
<div id="app">
    <!-- ./set loading layer -->
@yield('page-content')
<!-- javascript -->
</div>
<script src="{{ asset('js/backend/jquery.min.js') }}"></script>
<script src="{{ asset('js/backend/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/backend/bootstrap.min.js') }}"></script>

<script src="{{ asset('js/backend/jquery.knob.min.js') }}"></script>
<script src="{{ asset('js/backend/jquery.sparkline.min.js') }}"></script>

<script src="{{ asset('js/backend/bootstrap-select.js') }}"></script>

<script src="{{ asset('js/backend/waypoints.min.js') }}"></script>
<script src="{{ asset('js/backend/jquery.counterup.min.js') }}"></script>

<script src="{{ asset('js/backend/jquery.unveil.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>

<script src="{{ asset('js/backend/jquery.mCustomScrollbar.min.js') }}"></script>
<script src="{{ asset('js/backend/moment.js') }}"></script>

@stack('plugins')

{{--<script src="{{ asset('js/backend/dev-timeout.js') }}"></script>--}}
<script src="{{ asset('js/backend/dev-settings.js') }}"></script>
<script src="{{ asset('js/backend/dev-loaders.js') }}"></script>
<script src="{{ asset('js/backend/dev-layout-default.js') }}"></script>
<script src="{{ asset('js/backend/demo.js') }}"></script>
<script src="{{ asset('js/backend/dev-app.js') }}"></script>
{{--<script src="{{ asset('js/backend/demo-dashboard.js') }}"></script>--}}

{{--<script src="{{ asset('js/backend/demo-file-handling.js') }}"></script>--}}

@stack('scripts')

</body>
</html>
