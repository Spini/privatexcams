<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta section -->
    <title>Privatexcams</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- ./meta section -->

    <!-- css styles -->
    <link rel="stylesheet" type="text/css" href="css/backend/horizontal-blue-white.css" id="dev-css">
    <!-- ./css styles -->

    <!--[if lte IE 9]>
    <!--<link rel="stylesheet" type="text/css" href="css/backend/dev-ie-fix.css">-->
    <![endif]-->

    <!-- javascripts -->
    <script src="{{ asset('js/backend/modernizr.js') }}"></script>
    <!-- ./javascripts -->

    <style>
        .dev-page {
            visibility: hidden;
        }
    </style>
</head>
<body>
<!-- set loading layer -->
<div class="dev-page-loading preloader"></div>
<!-- ./set loading layer -->

<!-- page wrapper -->
<div class="dev-page">

    <!-- page header -->
    <div class="dev-page-header">

        <div class="dph-logo">
            <a href="{{ url('dashboard') }}">Intuitive</a>
        </div>

        <ul class="dph-buttons pull-right">
            <li class="dph-button-stuck">
                <a href="#" class="dev-page-navigation-toggle">
                    <span class="fa fa-bars"></span>
                </a>
            </li>
            <li class="dph-button-stuck">
                <a href="#" class="dev-page-search-toggle">
                    <div class="dev-page-search-toggle-icon">
                        <span class="circle"></span>
                        <span class="line"></span>
                    </div>
                </a>
            </li>
            <li class="dph-button-stuck">
                <a href="#" class="dev-page-rightbar-toggle">
                    <div class="dev-page-rightbar-toggle-icon">
                        <span class="line-one"></span>
                        <span class="line-two"></span>
                    </div>
                </a>
            </li>
        </ul>

        <ul class="dev-page-navigation">
            <li>
                <a href="{{ url('/studio') }}">Dashboard</a>
            </li>
            <li>
                <a href="#">Mail</a>
                <ul>
                    <li><a href="{{ url('#') }}">Compose</a></li>
                    <li><a href="{{ url('#') }}">Inbox</a></li>
                </ul>


            </li>

        </ul>

    </div>
    <!-- ./page header -->

    <!-- page container -->
    <div class="dev-page-container">

        <!-- page content -->
        <div class="dev-page-content">
            <!-- page content container -->
            <div class="container">
                @yield('content')
                @include('common.copyright')
            </div>
            <!-- ./page content container -->

        </div>
        <!-- ./page content -->
    </div>
    <!-- ./page container -->

    <!-- right bar -->
@include('common.rightbar')
<!-- ./right bar -->

    <!-- page footer -->
@include('common.footer')
<!-- ./page footer -->

    <!-- page search -->
@include('common.search')
<!-- ./page search -->

</div>
<!-- ./page wrapper -->

<!-- javascript -->
<script src="{{ asset('js/backend/jquery.min.js') }}"></script>
<script src="{{ asset('js/backend/bootstrap.min.js') }}"></script>

<script src="{{ asset('js/backend/jquery.mCustomScrollbar.min.js') }}"></script>
<script src="{{ asset('js/backend/moment.js') }}"></script>

<script src="{{ asset('js/backend/jquery.knob.min.js') }}"></script>
<script src="{{ asset('js/backend/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('js/backend/jquery.smartWizard.js') }}"></script>

<script src="{{ asset('js/backend/bootstrap-select.js') }}"></script>

<script src="{{ asset('js/backend/d3.min.js') }}"></script>
<script src="{{ asset('js/backend/nv.d3.min.js') }}"></script>
<script src="{{ asset('js/backend/stream_layers.js') }}"></script>

<script src="{{ asset('js/backend/waypoints.min.js') }}"></script>
<script src="{{ asset('js/backend/jquery.counterup.min.js') }}"></script>

<script src="{{ asset('js/backend/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('js/backend/jquery.blueimp-gallery.min.js') }}"></script>
<script src="{{ asset('js/backend/jquery.unveil.js') }}"></script>
<script src="{{ asset('js/backend/dropzone.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>

<script src="{{ asset('js/backend/dev-settings.js') }}"></script>
<script src="{{ asset('js/backend/dev-loaders.js') }}"></script>
<script src="{{ asset('js/backend/dev-layout-horizontal.js') }}"></script>
<script src="{{ asset('js/backend/demo.js') }}"></script>
<script src="{{ asset('js/backend/dev-app.js') }}"></script>
<script src="{{ asset('js/backend/demo-dashboard.js') }}"></script>
<!-- ./javascript -->
</body>
</html>






