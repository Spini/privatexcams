@extends('layouts.app')
@section('page-content')
    <div class="dev-page">
    @include('common.header')
    <div class="dev-page-container">
    @include('common.sidebar')
    <!-- page content -->
        <div class="dev-page-content">
            <!-- page content container -->
            <div class="container">
                @yield('content')
                @include('common.copyright')
            </div>
            <!-- ./page content container -->
        </div>
        <!-- ./page content -->
    </div>
    @include('common.rightbar')
    @include('common.footer')
    @include('common.search')
</div>
@endsection
