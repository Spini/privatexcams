<?php


if(!function_exists('is_url')){
	function is_url($url, $class='active'){
		 return url()->current() == $url ? $class : null;
	}
}