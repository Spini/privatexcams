<?php namespace App;


class Menu {

	public static function isUrl( $url, $class = 'active' ) {
		return url()->current() == $url ? $class : null;
	}

	public static function sideBar() {
		$sidebar_menu   = [];
		$user           = \Auth::user();
		$sidebar_menu[] = static::item( 'Dashboard', url( 'dashboard' ), 'fa fa-desktop' );
		switch ( $user->role->name ) {
			case 'studio':
				$sidebar_menu[] = static::item( 'Profile', url( 'studio' ), 'fa fa-desktop' );
				break;
			case 'model':
				$sidebar_menu[] = static::item( ' Messages <span
                                class="badge badge-info">2</span>', url( 'inbox' ), 'fa fa-comment' );
				$sidebar_menu[] = static::item( 'Broadcast now', url( 'live' ), 'fa fa-gears' );
				break;
			default:
				$sidebar_menu[] = static::item( 'Profile', url( 'user-show' ), 'fa fa-desktop' );
				$sidebar_menu[] = static::item( 'Favorite Models', url( 'favorites' ), 'fa  fa-gears' );
				$sidebar_menu[] = static::item( 'Live Models', url( '/' ), 'fa fa-gears' );
				$sidebar_menu[] = static::item( 'Add Credit', url( 'credits-add' ), 'fa fa-gears' );
				break;
		}

		$sidebar_menu[] = static::item( 'Schedule', url( 'schedule' ), 'fa  fa-gears' );
		$sidebar_menu[] = static::item( 'Lockscreen', url( 'lockscreen' ), 'fa  fa-gears' );


		return $sidebar_menu;
	}


	protected static function item( $display, $url, $icon = null ) {
		$item            = [];
		$item['display'] = $display;
		$item['url']     = $url;
		$item['icon']    = $icon;

		return $item;
	}
}