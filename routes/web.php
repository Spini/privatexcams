<?php

Route::group( [ 'prefix' => 'admin' ], function () {
	Voyager::routes();
} );

Auth::routes();
Frontend::routes();

Route::group( [ 'middleware' => 'auth' ], function () {
	Model::routes();
	Route::get( '/dashboard', function () {
		return view( 'dashboard' );
	} );
	Route::get( '/user-show', function () {
		return view( 'users.user.show' );
	} );
	Route::get( '/user-edit', function () {
		return view( 'users.user.edit' );
	} );
	Route::get( '/inbox', function () {
		return view( 'mail.index' );
	} );
	Route::get( '/sent', function () {
		return view( 'mail.sent' );
	} );
	Route::get( '/trash', function () {
		return view( 'mail.trash' );
	} );
	Route::get( '/compose', function () {
		return view( 'mail.create' );
	} );
	Route::get( '/friends', function () {
		return view( 'users.user.friends' );
	} );
	Route::get( '/gallery', function () {
		return view( 'users.user.gallery' );
	} );
	Route::get( '/videos', function () {
		return view( 'users.user.videos' );
	} );
	Route::get( '/credits-show', function () {
		return view( 'users.credits.show' );
	} );
	Route::get( '/credits-add', function () {
		return view( 'users.credits.edit' );
	} );
	Route::get( '/gifts-show', function () {
		return view( 'users.gifts.show' );
	} );
	Route::get( '/gifts-add', function () {
		return view( 'users.gifts.index' );
	} );
	Route::get( '/settings', function () {
		return view( 'users.settings' );
	} );
	Route::get( '/profile', function () {
		return view( 'users.profile' );
	} );
	Route::get( '/live', function () {
		return view( 'pages.broadcast' );
	} );
	Route::get( '/lockscreen', function () {
		return view( 'auth.lockscreen' );
	} );
	Route::get( '/favorites', function () {
		return view( 'users.user.favorite' );
	} );
	Route::get( '/schedule', function () {
		return view( 'pages.schedule' );
	} );
	Route::get( '/studio', function () {
		return view( 'users.studio.dashboard' );
	} );


	Route::get( '/home', 'HomeController@index' );
} );



