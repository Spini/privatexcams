@extends('frontend::layouts.app')
@section('content')
    <div class="aboutBG hide-for-small">

    </div>
    <div class="contentImageHolder hide-for-small">

    </div>
    <div class="contentHolder transparent scrollContent">
        <div class="pageTitle">
            <h2>Contact <span>Us</span></h2>
            <h4>Get in touch</h4>
        </div>


        <div class="aboutFull full contact">
            <h3>We <span>are</span> here</h3>
            <p>
                <iframe width="600" height="350"
                        src="https://www.google.com/maps?q=51.541211,-0.123811&amp;num=1&amp;ie=UTF8&amp;t=m&amp;z=10&amp;iwloc=A&amp;output=embed"></iframe>
            </p>
        </div>

        <div class="full contactForm">
            <div class="aboutFull full">
                <div class="half first">
                    <input type="text" name="fname" value="Name"
                           onfocus="if(this.value  == 'Name') { this.value = ''; } "
                           onblur="if(this.value == '') { this.value = 'Name'; } ">
                </div>

                <div class="half second">
                    <input type="text" name="email" value="Email"
                           onfocus="if(this.value  == 'Email') { this.value = ''; } "
                           onblur="if(this.value == '') { this.value = 'Email'; } ">
                </div>
                <div class="row">
                     <textarea onfocus="if(this.value == 'Message') this.value='';"
                               onblur="if(this.value == '') this.value='Message';">Message</textarea>
                </div>
                <div class="row">
                    <a href="#" class="sndBtn" style="padding-top: 15px !important">Send Message</a>
                </div>
            </div>

        </div>

        <div class="footerItem white">
            <div class="socials">
                <a href="#" class="facebook"></a>
                <a href="#" class="twitter"></a>
                <a href="#" class="vimeo"></a>
                <a href="#" class="reddit"></a>
                <a href="#" class="dribbble"></a>
                <a href="#" class="behance"></a>
                <a href="#" class="linkedin"></a>
                <a href="#" class="rss"></a>
                <a href="/" id="toTop" class="toTop">Back to top</a>
            </div>
            <ul class="links">
                <li>website by <a href="http://loopbytes.com/">loopBytes</a></li>
                <li>all rights reserved 2017</li>
                <li><a href="#">model login</a></li>
                <li><a href="#">model wanted</a></li>
                <li><a href="#">terms and conditions</a></li>
                <li><a href="#">privacy policy</a></li>
                <li><a href="{{ url('contact') }}">contact us</a></li>
            </ul>
        </div>
    </div>
@endsection