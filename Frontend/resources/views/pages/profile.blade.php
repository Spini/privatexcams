@extends('frontend::layouts.app')
@section('content')
    <div class="profile contentHolder transparent scrollContent transparentAbout"
         style=" background-color: rgba(0, 0, 0, 0.5) !important;">
        <div class="full"><a class="category" style="float: right;margin-top: -30px;margin-right: -30px;">
                <img src="img/frontend/closing.png" alt="">
            </a>
        </div>
        <div class="full" style="margin-top: 20px">
            <div class="aboutHalf half" style="width: 75%">
                <div class="aboutFull full">
                    <video width="100%" controls="controls">
                        <source src="http://privatex.app/img/backend/Sinner.mp4" type="video/mp4">
                    </video>
                </div>
            </div>
            <div class="aboutHalf half" style="width: 25%; height: inherit">
                <div class="aboutFull full slider">
                    <h3>Who <span>we</span> are</h3>
                    <p>Egestas porta odio? Elementum, elementum. Elit magna enim facilisis, sed duis. Odio et
                        elementum
                        nascetur, sit diam ultrices nunc, sit sed, eu in! Dolor sit adipiscing penatibus, proin
                        ridiculus
                        aenean turpis nec non cursus magna purus, massa proin, enim? Sed integer natoque elementum
                        habitasse, pellentesque? Ac mattis mattis aliquam, augue, vel porta sagittis quis, dignissim
                        aeanean
                        eu! Nec scelerisque, auctor augue, etiam odio nec! Et nisi, aenean purus ultricies? Hac ac?
                        Ultricies vel ut, mauris. Dis tortor nisi nunc.
                        Egestas porta odio? Elementum, elementum. Elit magna enim facilisis, sed duis. Odio et
                        elementum
                        nascetur, sit diam ultrices nunc, sit sed, eu in! Dolor sit adipiscing penatibus, proin
                        ridiculus
                        aenean turpis nec non cursus magna purus, massa proin, enim? Sed integer natoque elementum
                        habitasse, pellentesque? Ac mattis mattis aliquam, augue, vel porta sagittis quis, dignissim
                        aeanean
                        eu! Nec scelerisque, auctor augue, etiam odio nec! Et nisi, aenean purus ultricies? Hac ac?
                        Ultricies vel ut, mauris. Dis tortor nisi nunc.Egestas porta odio? Elementum, elementum.
                        Elit
                        magna enim facilisis, sed duis. Odio et elementum
                        nascetur, sit diam ultrices nunc, sit sed, eu in! Dolor sit adipiscing penatibus, proin
                        ridiculus
                        aenean turpis nec non cursus magna purus, massa proin, enim? Sed integer natoque elementum
                        habitasse, pellentesque? Ac mattis mattis aliquam, augue, vel porta sagittis quis, dignissim
                        aeanean
                        eu! Nec scelerisque, auctor augue, etiam odio nec! Et nisi, aenean purus ultricies? Hac ac?
                        Ultricies vel ut, mauris. Dis tortor nisi nunc.Egestas porta odio?</p>
                </div>
            </div>
            <div class="aboutHalf teamFull slider half" style="width: 35%">
                <a class="teamImgHolder" style="float: left">
                    <img src="img/frontend/image1.jpg" alt="">
                </a>
                <div class="contentTeam">
                    <div class="pageTitle" style="margin-bottom: 5px">
                        <h2><span> Angel Love</span></h2>
                    </div>
                    <div class="row" style="text-align: center">
                        <div class="col-md-3">
                            <h3>Female</h3>
                            <p>Gender</p>
                        </div>
                        <div class="col-md-3">
                            <h3>22</h3>
                            <p>Age</p></div>
                        <div class="col-md-3">
                            <h3>Big</h3>
                            <p>Breast</p></div>
                        <div class="col-md-3">
                            <h3>Latino</h3>
                            <p>Ethnicity</p>
                        </div>
                    </div>
                    <div class="socials">
                        <a href="#" target="_blank" class="facebook"></a>
                        <a href="#" target="_blank" class="twitter"></a>
                        <a href="#" target="_blank" class="reddit"></a>
                        <a href="#" target="_blank" class="dribble"></a>
                        <a href="#" target="_blank" class="vimeo"></a>
                    </div>
                </div>
            </div>
            <div class="aboutHalf half" style="width: 65%">
                <div class="aboutFull full slider">
                    <h3>Who <span>we</span> are</h3>
                    <p>Egestas porta odio? Elementum, elementum. Elit magna enim facilisis, sed duis. Odio et
                        elementum
                        nascetur, sit diam ultrices nunc, sit sed, eu in! Dolor sit adipiscing penatibus, proin
                        ridiculus
                        aenean turpis nec non cursus magna purus, massa proin, enim? Sed integer natoque elementum
                        habitasse, pellentesque? Ac mattis mattis aliquam, augue, vel porta sagittis quis, dignissim
                        aeanean
                        eu! Nec scelerisque, auctor augue, etiam odio nec! Et nisi, aenean purus ultricies? Hac ac?
                        Ultricies vel ut, mauris. Dis tortor nisi nunc.
                        Egestas porta odio? Elementum, elementum. Elit magna enim facilisis, sed duis. Odio et
                        elementum
                        nascetur, sit diam ultrices nunc, sit sed, eu in! Dolor sit adipiscing penatibus, proin
                        ridiculus
                        aenean turpis nec non cursus magna purus, massa proin, enim? Sed integer natoque elementum
                        habitasse, pellentesque? Ac mattis mattis aliquam, augue, vel porta sagittis quis, dignissim
                        aeanean
                        eu! Nec scelerisque, auctor augue, etiam odio nec! Et nisi, aenean purus ultricies? Hac ac?
                        Ultricies vel ut, mauris. Dis tortor nisi nunc.Egestas porta odio? Elementum, elementum.
                        Elit
                        magna enim facilisis, sed duis. Odio et elementum
                        nascetur, sit diam ultrices nunc, sit sed, eu in!</p>
                </div>
            </div>
            <div class="aboutFull full slider"  style="height: 100%;">
                <div id="container" style="margin-left: 0px; margin-right: 0px;">
                    <div class="item red">
                        <div class="shadow">
                            <img src="http://placehold.it/307x356/ae8181/ffffff.jpg" alt="">
                        </div>
                    </div>

                    <div class="item red">
                        <div class="shadow">
                            <img src="http://placehold.it/307x497/ae8181/ffffff.jpg" alt="">
                        </div>
                    </div>

                    <div class="item red">
                        <div class="shadow">
                            <img src="http://placehold.it/307x357/ae8181/ffffff.jpg" alt="">
                        </div>
                    </div>

                    <div class="item red">
                        <div class="shadow">
                            <img src="http://placehold.it/307x359/ae8181/ffffff.jpg" alt="">
                        </div>
                    </div>

                    <div class="item red">
                        <div class="shadow">
                            <img src="http://placehold.it/307x588/ae8181/ffffff.jpg" alt="">
                        </div>
                    </div>

                    <div class="item red">
                        <div class="shadow">
                            <img src="http://placehold.it/307x355/ae8181/ffffff.jpg" alt="">
                        </div>
                    </div>

                    <div class="item red">
                        <div class="shadow">
                            <img src="http://placehold.it/307x501/ae8181/ffffff.jpg" alt="">
                        </div>
                    </div>

                    <div class="item red">
                        <div class="shadow">
                            <img src="http://placehold.it/307x469/ae8181/ffffff.jpg" alt="">
                        </div>
                    </div>

                    <div class="item red">
                        <div class="shadow">
                            <img src="http://placehold.it/307x469/ae8181/ffffff.jpg" alt="">
                        </div>
                    </div>

                    <div class="item red">
                        <div class="shadow">
                            <img src="http://placehold.it/307x356/ae8181/ffffff.jpg" alt="">
                        </div>
                    </div>

                    <div class="item red">
                        <div class="shadow">
                            <img src="http://placehold.it/307x356/ae8181/ffffff.jpg" alt="">
                        </div>
                    </div>

                    <div class="item red">
                        <div class="shadow">
                            <img src="http://placehold.it/307x356/ae8181/ffffff.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection