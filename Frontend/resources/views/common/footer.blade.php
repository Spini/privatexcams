<div id="container">
    <div class="item footerItem">

        <div class="socials">
            <a href="#" class="facebook"></a>
            <a href="#" class="twitter"></a>
            <a href="#" class="vimeo"></a>
            <a href="#" class="reddit"></a>
            <a href="#" class="dribbble"></a>
            <a href="#" class="behance"></a>
            <a href="#" class="linkedin"></a>
            <a href="#" class="rss"></a>
            <a href="/" id="toTop" class="toTop">Back to top</a>
        </div>
        <ul class="links">
            <li>website by <a href="http://loopbytes.com/">loopBytes</a></li>
            <li>all rights reserved 2017</li>
            <li><a href="#">model login</a></li>
            <li><a href="#">model wanted</a></li>
            <li><a href="/terms">terms and conditions</a></li>
            <li><a href="/privacy">privacy policy</a></li>
            <li><a href="/contact">contact us</a></li>
        </ul>
    </div>
</div>