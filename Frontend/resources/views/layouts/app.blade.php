<!DOCTYPE html>
<html class="no-js lt-ie9 lt-ie8 lt-ie7">
<html class="no-js lt-ie9 lt-ie8">
<html class="no-js lt-ie9">
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>Privatexcams</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="stylesheet" href="{{ asset('css/backend/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/frontend/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/frontend/child.css') }}">
{{--<script src="{{ asset('js/frontend/modernizr-2.6.2.min.js') }}"></script>--}}

<!--Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'> <!--Lato-->

</head>

<body>
<div id="spinner">
    <img src="{{ asset('img/frontend/logosmall.png') }}" alt="img" class="logo">
    <img src="{{ asset('img/frontend/line.png') }}" alt="img" class="line">
</div>

<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
    improve your experience.</p>
<![endif]-->

@include('frontend::common.navbar')
@yield('content')
@include('frontend::common.footer')
@yield('modals')

<script src="{{ asset('js/backend/jquery.min.js') }}"></script>
<script src="{{ asset('js/backend/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/frontend/main.js') }}"></script>
<script>
    $(function () {
        $('#dl-menu').dlmenu();
    });
</script>

<script>
    $(document).foundation();
</script>

</body>
</html>
