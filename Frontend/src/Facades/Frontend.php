<?php namespace Frontend\Facades;


use Illuminate\Support\Facades\Facade;

class Frontend extends Facade {

	protected static function getFacadeAccessor() {
		return 'frontend';
	}

}