<?php namespace Frontend\Services;


class FrontendService {


	public function routes() {
		require $this->path('routes/web.php');
	}

	public function path( $path = null ) {
		return realpath(__DIR__ . '/../../' . $path);
	}
}