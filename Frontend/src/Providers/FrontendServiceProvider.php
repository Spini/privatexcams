<?php namespace Frontend\Providers;


use Frontend\Services\FrontendService;
use Illuminate\Support\ServiceProvider;

class FrontendServiceProvider extends ServiceProvider {

	public function boot() {
		$this->loadViewsFrom( $this->path('resources/views'), 'frontend' );
	}

	public function register() {
		$this->app->singleton('frontend', function (){
			return new FrontendService();
		});
	}

	protected function path( $path = null ) {
		// frontend/resources/
		// D:/users/www/

		return realpath( __DIR__ . '/../../' . $path );
	}
}