<?php


Route::get( '/', function () {

	return view( 'frontend::welcome' );
} );
/*Route::get( '/login', function () {
	return view( 'auth.login' );
} );
Route::get( '/register', function () {
	return view('auth.register' );
} );*/
Route::get( '/allmodels', function () {
	return view( 'frontend::pages.models' );
} );
Route::get( '/test', function () {
	return view( 'frontend::pages.profile' );
} );
Route::get( '/contact', function () {
	return view( 'frontend::pages.contact' );
} );
Route::get( '/terms', function () {
	return view( 'frontend::pages.terms' );
} );
Route::get( '/privacy', function () {
	return view( 'frontend::pages.privacy' );
} );
