<?php namespace Model\Facades;


use Illuminate\Support\Facades\Facade;

class Model extends Facade {

	protected static function getFacadeAccessor() {
		return 'model';
	}

}