<?php namespace Model\Providers;

use Illuminate\Support\ServiceProvider;
use Model\Services\ModelService;

class ModelServiceProvider extends ServiceProvider {

	public function boot() {
		$this->loadViewsFrom( $this->path('resources/views'), 'model' );
	}

	public function register() {
		$this->app->singleton('model', function (){
			return new ModelService();
		});
	}

	protected function path( $path = null ) {
		return realpath( __DIR__ . '/../../' . $path );
	}
}