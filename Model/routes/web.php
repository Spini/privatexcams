<?php

Route::get( '/dashboard', function () {
	return view( 'model::profile.dashboard' );
} );
Route::get( '/show', function () {
	return view( 'model::profile.show' );
} );
Route::get( '/edit', function () {
	return view( 'model::profile.edit' );
} );
Route::get( '/my-schedule', function () {
	return view( 'model::pages.schedule' );
} );



