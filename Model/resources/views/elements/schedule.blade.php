<div id="page-container">
    <!-- Main Container -->
    <main id="main-container">
        <!-- Page Header -->
        <div class="content bg-gray-lighter">
            <div class="row items-push">
                <div class="col-sm-8">
                    <h1 class="page-heading">
                        Model schedule
                        <small>See her schedule here!
                        </small>
                    </h1>
                </div>
            </div>
        </div>
        <!-- END Page Header -->

        <!-- Page Content -->
        <div class="content bg-white">
            <!-- Calendar and Events functionality (initialized in js/pages/base_comp_calendar.js), for more info and examples you can check out http://fullcalendar.io/ -->
            <div class="row items-push">
                <div class="js-calendar"></div>
            </div>
            <!-- END Calendar -->
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->
</div>
@push('css')
<link rel="stylesheet" href="css/backend/fullcalendar.min.css">
<link rel="stylesheet" id="css-main" href="css/backend/oneui.min.css">
@endpush
@push('plugins')
<script src="{{ asset('js/backend/fullcalendar.min.js') }}"></script>
<script src="{{ asset('js/backend/gcal.min.js') }}"></script>
<script src="{{ asset('js/backend/base_comp_calendar.js') }}"></script>
@endpush