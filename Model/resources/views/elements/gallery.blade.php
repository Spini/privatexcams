@extends('profile.show')
@section('content')
    <ul class="nav nav-tabs nav-tabs-arrowed" role="tablist">
        <li class="active"><a href="#tab1-first" role="tab" data-toggle="tab">Photos</a></li>
        <li><a href="#" role="tab" data-toggle="tab">Videos</a></li>
        <li><a href="#" role="tab" data-toggle="tab">Wish</a></li>
        <li><a href="#" role="tab" data-toggle="tab">Gifts</a></li>
        <li><a href="#" role="tab" data-toggle="tab">Schedule</a></li>
        <li><a href="#" role="tab" data-toggle="tab">Profile</a></li>
    </ul>
    <div class="panel-body tab-content">
        <div class="tab-pane active" id="tab1-first">
            <div class="gallery isotope" id="gallery">
                <div class="gallery-sizer"></div>
                <div class="gallery-gutter"></div>

                <a href="img/backend/img_10.jpg" class="gallery-item">
                    <img src="img/backend/img_10.jpg">
                    <div class="gallery-item-info">
                        <img src="img/backend/user_1.jpg"> Devin Stephens
                        <span class="pull-right">1h ago <i class="fa fa-clock-o"></i></span>
                    </div>
                </a>

                <a href="img/backend/img_11.jpg" class="gallery-item gallery-item-2">
                    <img src="img/backend/img_11.jpg">
                    <div class="gallery-item-info">
                        <img src="img/backend/user_1.jpg"> Devin Stephens
                        <span class="pull-right">2h ago <i class="fa fa-clock-o"></i></span>
                    </div>
                </a>

                <a href="img/backend/img_2.jpg" class="gallery-item">
                    <img src="img/backend/img_2.jpg">
                    <div class="gallery-item-info">
                        <img src="img/backend/user_1.jpg"> Devin Stephens
                        <span class="pull-right">1 day ago <i class="fa fa-clock-o"></i></span>
                    </div>
                </a>

                <a href="img/backend/img_8.jpg" class="gallery-item">
                    <img src="img/backend/img_8.jpg">
                    <div class="gallery-item-info">
                        <img src="img/backend/user_1.jpg"> Devin Stephens
                        <span class="pull-right">2 day ago <i class="fa fa-clock-o"></i></span>
                    </div>
                </a>

                <a href="img/backend/img_3.jpg" class="gallery-item">
                    <img src="img/backend/img_3.jpg">
                    <div class="gallery-item-info">
                        <img src="img/backend/user_1.jpg"> Devin Stephens
                        <span class="pull-right">1 week ago <i class="fa fa-clock-o"></i></span>
                    </div>
                </a>

                <a href="img/backend/img_4.jpg" class="gallery-item">
                    <img src="img/backend/img_4.jpg">
                    <div class="gallery-item-info">
                        <img src="img/backend/user_1.jpg"> Devin Stephens
                        <span class="pull-right">1 week ago <i class="fa fa-clock-o"></i></span>
                    </div>
                </a>

                <a href="img/backend/img_5.jpg" class="gallery-item">
                    <img src="img/backend/img_5.jpg">
                    <div class="gallery-item-info">
                        <img src="img/backend/user_1.jpg"> Devin Stephens
                        <span class="pull-right">1 week ago <i class="fa fa-clock-o"></i></span>
                    </div>
                </a>

                <a href="img/backend/img_6.jpg" class="gallery-item">
                    <img src="img/backend/img_6.jpg">
                    <div class="gallery-item-info">
                        <img src="img/backend/user_1.jpg"> Devin Stephens
                        <span class="pull-right">1 week ago <i class="fa fa-clock-o"></i></span>
                    </div>
                </a>

                <a href="img/backend/img_7.jpg" class="gallery-item">
                    <img src="img/backend/img_7.jpg">
                    <div class="gallery-item-info">
                        <img src="img/backend/user_1.jpg"> Devin Stephens
                        <span class="pull-right">1 week ago <i class="fa fa-clock-o"></i></span>
                    </div>
                </a>

                <a href="img/backend/img_1.jpg" class="gallery-item">
                    <img src="img/backend/img_1.jpg">
                    <div class="gallery-item-info">
                        <img src="img/backend/user_1.jpg"> Devin Stephens
                        <span class="pull-right">1 week ago <i class="fa fa-clock-o"></i></span>
                    </div>
                </a>

                <a href="img/backend/img_9.jpg" class="gallery-item">
                    <img src="img/backend/img_9.jpg">
                    <div class="gallery-item-info">
                        <img src="img/backend/user_1.jpg"> Devin Stephens
                        <span class="pull-right">2 weeks ago <i class="fa fa-clock-o"></i></span>
                    </div>
                </a>

            </div>
        </div>
    </div>
@endsection

@push('plugins')
<script src="{{ asset('js/backend/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('js/backend/jquery.blueimp-gallery.min.js') }}"></script>
<script src="{{ asset('js/backend/jquery.unveil.js') }}"></script>
@endpush

@push('scripts')
<script type="text/javascript">
    if (document.images) {
        img1 = new Image();
        img2 = new Image();
        img3 = new Image();
        img4 = new Image();
        img5 = new Image();
        img6 = new Image();
        img7 = new Image();
        img8 = new Image();
        img9 = new Image();
        img10 = new Image();

        img1.src = "img/backend/img_1.jpg";
        img2.src = "img/backend/img_2.jpg";
        img3.src = "img/backend/img_3.jpg";
        img4.src = "img/backend/img_4.jpg";
        img5.src = "img/backend/img_5.jpg";
        img6.src = "img/backend/img_6.jpg";
        img7.src = "img/backend/img_7.jpg";
        img8.src = "img/backend/img_8.jpg";
        img9.src = "img/backend/img_9.jpg";
        img10.src = "img/backend/img_10.jpg";
    }
</script>
<script>

    $(function () {
        setTimeout(function () {

            $("img").unveil(200);

            $('.gallery.isotope').isotope({
                itemSelector: '.gallery-item',
                percentPosition: true,
                masonry: {
                    columnWidth: '.gallery-sizer',
                    gutter: '.gallery-gutter'
                }
            });

            $(".dev-page-sidebar-collapse, .dev-page-sidebar-minimize").on("click", function () {
                setTimeout(function () {
                    $('.gallery.isotope').isotope('layout');
                    dev_layout_alpha_content.init(dev_layout_alpha_settings);
                }, 300);
            });

            document.getElementById('gallery').onclick = function (event) {
                event = event || window.event;
                var target = event.target || event.srcElement,
                    link = target.src ? target.parentNode : target,
                    options = {index: link, event: event},
                    links = this.getElementsByTagName('a');
                blueimp.Gallery(links, options);
            };

        }, 200);

    });
</script>
@endpush