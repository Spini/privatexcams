const {mix} = require('laravel-mix');

mix.scripts([
    'Frontend/resources/assets/js/vendor/jquery-1.9.1.min.js',
    'Frontend/resources/assets/js/plugins/plugins.js',
    'Frontend/resources/assets/js/main-frontend.js',
    'Frontend/resources/assets/js/plugins/jquery.isotope.min.js',
    'Frontend/resources/assets/js/plugins/modernizr.custom.js',
    'Frontend/resources/assets/js/plugins/toucheffects.js',
    'Frontend/resources/assets/js/plugins/jquery.prettyPhoto.js',
    'Frontend/resources/assets/js/plugins/foundation.min.js',
    'Frontend/resources/assets/js/plugins/jquery.dlmenu.js',
    'Frontend/resources/assets/js/plugins/spinner.js',
    'Frontend/resources/assets/js/plugins/prettyphoto.js',
    'Frontend/resources/assets/js/plugins/scrolltop.js',
    'Frontend/resources/assets/js/plugins/masonry.js',
    'Frontend/resources/assets/js/plugins/search-filters.js',
], 'public/js/frontend/main.js')
    .js('Frontend/resources/assets/js/vendor/modernizr-2.6.2.min.js', 'public/js/frontend')
    .styles(['Frontend/resources/assets/css/normalize.css',
            'Frontend/resources/assets/css/main.css',
            'Frontend/resources/assets/css/foundation.min.css',
            'Frontend/resources/assets/css/iso-style.css',
            'Frontend/resources/assets/css/jquery.bxslider.css',
            'Frontend/resources/assets/css/jquery.mCustomScrollbar.css',
            'Frontend/resources/assets/css/component.css',
            'Frontend/resources/assets/css/popup-filters.css',
            'Frontend/resources/assets/css/popup-search.css',
            'Frontend/resources/assets/css/prettyPhoto.css',
            'Frontend/resources/assets/css/style.css',        ],
        'public/css/frontend/main.css')
    .copy('Frontend/resources/assets/img', 'public/img/frontend')

    .styles([
        'resources/assets/css/dev-plugins/mcustomscrollbar/jquery.mCustomScrollbar.css',
        'resources/assets/css/dev-plugins/blueimp/blueimp-gallery.min.css',
        'resources/assets/css/dev-plugins/codemirror/codemirror.css',
        'resources/assets/css/default-dark.css'
    ],'public/css/default-dark.css')
    .js('resources/assets/js/app.js', 'public/js')
    .copy('resources/assets/fonts','public/css/backend/dev-plugins/fonts')
    .copy('resources/assets/css','public/css/backend')
    .copy('resources/assets/less','public/less')
    .copy('resources/assets/img','public/img/backend')
    .copy('resources/assets/js','public/js/backend')

;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
